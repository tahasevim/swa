module github.com/nursena/SWA/services/course

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.11
	github.com/lib/pq v1.3.0
)
