package controller

import (
	"fmt"
	"net/http"

	"github.com/nursena/SWA/services/course/database"
	"github.com/nursena/SWA/services/course/models"
)

func GetDepartmentCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting department courses")
	departmentID := r.Header.Get("departmentID")
	defer r.Body.Close()
	department, _ := database.GetDepartmentInfo(departmentID)
	fmt.Println(department)

	shownDepartmentCourse, err := database.GetDepartmentMustCourses(department.DepartmentCourseIDs)
	fmt.Println(shownDepartmentCourse, err)

	var obj models.DepartmentCoursesReturn
	obj.ShownDepartmentCourses = shownDepartmentCourse

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, obj)
}

func GetElectiveCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting elective courses")
	departmentID := r.Header.Get("departmentID")
	defer r.Body.Close()
	department, _ := database.GetDepartmentInfo(departmentID)
	fmt.Println(department)

	shownOtherElectiveCourses, err := database.GetDepartmentOtherElectiveCourses(department.OtherElectiveCourseIDs)
	fmt.Println(shownOtherElectiveCourses, err)

	shownCommonElectiveCourses, err := database.GetCommonElectiveCourses()
	fmt.Println(shownCommonElectiveCourses, err)

	var obj models.ElectiveCoursesReturn
	obj.ShownOtherElectiveCourses = shownOtherElectiveCourses
	obj.ShownCommonElectiveCourses = shownCommonElectiveCourses

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, obj)

}

func GetDepartmentAndAdvisorInformation(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting department information")
	departmentID := r.Header.Get("departmentID")
	advisorID := r.Header.Get("advisorID")
	fmt.Println(departmentID)
	fmt.Println(advisorID)

	defer r.Body.Close()

	department, err := database.GetDepartmentInfo(departmentID)
	advisor, err := database.GetAdvisorInfo(advisorID)
	fmt.Println(department)
	fmt.Println(advisor)

	var obj models.DeparmentAndAdvisorReturn
	obj.Advisor = advisor
	obj.Department = department

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, obj)

}

func GetCourseSections(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting course sections")
	courseID := r.Header.Get("courseID")
	fmt.Println(courseID)
	defer r.Body.Close()

	sectionIDs := database.GetSectionIDs(courseID).SectionIDs
	courseSections, err := database.GetCourseSectionsData(sectionIDs)

	fmt.Println(courseSections, err)
	var obj models.CourseSectionsReturn
	obj.CourseSections = courseSections

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, obj)
}
