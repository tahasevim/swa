package models

import (
	"github.com/lib/pq"
)

type ShownDepartmentCourse struct {
	DepartmentCourseID string         `json:"departmentCourseID"`
	CourseID           string         `json:"courseID"`
	CourseCode         string         `json:"courseCode"`
	OpticCode          string         `json:"opticCode"`
	CourseName         string         `json:"courseName"`
	TheoryHour         int            `json:"theoryHour"`
	PraticsHour        int            `json:"praticsHour"`
	Credit             int            `json:"credit"`
	Akts               int            `json:"akts"`
	SectionIDs         pq.StringArray `json:"sectionIDs"`
	IsMust             bool           `json:"isMust"`
	Term               string         `json:"term"`
}

type ShownOtherElectiveCourse struct {
	OtherElectiveCourseID string         `json:"otherElectiveCourseID"`
	CourseID              string         `json:"courseID"`
	CourseCode            string         `json:"courseCode"`
	OpticCode             string         `json:"opticCode"`
	CourseName            string         `json:"courseName"`
	TheoryHour            int            `json:"theoryHour"`
	PraticsHour           int            `json:"praticsHour"`
	Credit                int            `json:"credit"`
	Akts                  int            `json:"akts"`
	SectionIDs            pq.StringArray `json:"sectionIDs"`
	Term                  string         `json:"term"`
}

type ShownCourseSection struct {
	SectionID      string `json:"sectionID"`
	Quota          int    `json:"quota"`
	RemainingQuota int    `json:"remainingQuota"`
	Section        int    `json:"section"`
	Name           string `json:"teacherName"`
	Surname        string `json:"teacherSurname"`
	AcademicRank   string `json:"teacherAcademicRank"`
}

// type ShownCommonElectiveCourse struct {
// 	OtherElectiveCourseID string `json:"otherElectiveCourseID"`
// 	CourseCode            string `json:"courseCode"`
// 	OpticCode             string `json:"opticCode"`
// 	CourseName            string `json:"courseName"`
// 	TheoryHour            int    `json:"theoryHour"`
// 	PraticsHour           int    `json:"praticsHour"`
// 	Credit                int    `json:"credit"`
// 	Akts                  int    `json:"akts"`
// 	Term                  string `json:"term"`
// }

type Course struct {
	CourseID         string         `json:"courseID"`
	CourseCode       string         `json:"courseCode"`
	OpticCode        string         `json:"opticCode"`
	CourseName       string         `json:"courseName"`
	TheoryHour       int            `json:"theoryHour"`
	PraticsHour      int            `json:"praticsHour"`
	Credit           int            `json:"credit"`
	Akts             int            `json:"akts"`
	SectionIDs       pq.StringArray `gorm:"type:varchar(128)[]" json:"sectionIDs"`
	IsCommonElective bool           `json:"isCommonElective"`
}

type SectionRetrieval struct {
	SectionIDs pq.StringArray `gorm:"type:varchar(128)[]" json:"sectionIDs"`
}

type Section struct {
	SectionID      string `json:"sectionID"`
	Quota          int    `json:"quota"`
	RemainingQuota int    `json:"remainingQuota"`
	Section        int    `json:"section"`
	TeacherID      string `json:"teacherID"`
}

type DepartmentCourse struct {
	DepartmentCourseID string `json:"departmentCourseID"`
	CourseID           string `json:"courseID"`
	IsMust             bool   `json:"isMust"`
	Term               string `json:"term"`
}

type OtherElectiveCourse struct {
	OtherElectiveCourseID string `json:"otherElectiveCourseID"`
	CourseID              string `json:"courseID"`
	Term                  string `json:"term"`
}

type Department struct {
	DepartmentID           string         `json:"departmentID"`
	DepartmentCode         string         `json:"departmentCode"`
	DepartmentName         string         `json:"departmentName"`
	FacultyName            string         `json:"facultyName"`
	DepartmentCourseIDs    pq.StringArray `gorm:"type:varchar(128)[]" json:"departmentCourseIDs"`
	OtherElectiveCourseIDs pq.StringArray `gorm:"type:varchar(128)[]" json:"otherElectiveCourseIDs"`
}

type AddedDepartmentCourse struct {
	AddedDepartmentCourseID string `json:"addedDepartmentCourseID"`
	DepartmentCourseID      string `json:"departmentCourseID"`
	Section                 int    `json:"section"`
}

type AddedOtherElectiveCourse struct {
	AddedOtherElectiveCourseID string `json:"addedOtherElectiveCourseID"`
	OtherElectiveCourseID      string `json:"otherElectiveCourseID"`
	Section                    int    `json:"section"`
}

type AddedCommonElectiveCourse struct {
	AddedCommonElectiveCourseID string `json:"addedCommonElectiveCourseID"`
	CourseID                    string `json:"courseID"`
	Section                     int    `json:"section"`
}

type TakenDepartmentCourse struct {
	TakenDepartmentCourseID string `json:"takenDepartmentCourseID"`
	DepartmentCourseID      string `json:"departmentCourseID"`
	FinalGrade              string `json:"FinalGrade"`
	MakeUpGrade             string `json:"makeUpGrade"`
	IsPassed                bool   `json:"isPassed"`
}

type TakenOtherElectiveCourse struct {
	TakenOtherElectiveCourseID string `json:"takenOtherElectiveCourseID"`
	OtherElectiveCourseID      string `json:"otherElectiveCourseID"`
	FinalGrade                 string `json:"finalGrade"`
	MakeUpGrade                string `json:"makeUpGrade"`
	IsPassed                   bool   `json:"isPassed"`
}

type TakenCommonElectiveCourse struct {
	TakenCommonElectiveCourseID string `json:"takenCommonElectiveCourseID"`
	CourseID                    string `json:"courseID"`
	FinalGrade                  string `json:"finalGrade"`
	MakeUpGrade                 string `json:"makeUpGrade"`
	IsPassed                    bool   `json:"isPassed"`
	Term                        string `json:"term"`
}

type Teacher struct {
	TeacherID    string `json:"teacherID"`
	Name         string `json:"name"`
	Surname      string `json:"surname"`
	AcademicRank string `json:"academicRank"`
}

type CourseSectionsReturn struct {
	CourseSections []ShownCourseSection `json:"shownCourseSections"`
}

type DeparmentAndAdvisorReturn struct {
	Department Department `json:"department"`
	Advisor    Teacher    `json:"advisor"`
}

type DepartmentCoursesReturn struct {
	ShownDepartmentCourses []ShownDepartmentCourse `json:"shownDepartmentCourses"`
}

type ElectiveCoursesReturn struct {
	ShownOtherElectiveCourses  []ShownOtherElectiveCourse `json:"shownOtherElectiveCourses"`
	ShownCommonElectiveCourses []Course                   `json:"shownCommonElectiveCourses"`
}

type Courses struct {
	Courses []Course `json:"courses"`
}

type Sections struct {
	Sections []Section `json:"sections"`
}

type DepartmentCourses struct {
	DepartmentCourses []DepartmentCourse `json:"department_courses"`
}

type OtherElectiveCourses struct {
	OtherElectiveCourses []OtherElectiveCourse `json:"other_elective_courses"`
}

type Departments struct {
	Departments []Department `json:"departments"`
}

type TakenDepartmentCourses struct {
	TakenDepartmentCourses []TakenDepartmentCourse `json:"taken_department_courses"`
}

type TakenOtherElectiveCourses struct {
	TakenOtherElectiveCourses []TakenOtherElectiveCourse `json:"taken_other_elective_courses"`
}

type TakenCommonElectiveCourses struct {
	TakenCommonElectiveCourses []TakenCommonElectiveCourse `json:"taken_common_elective_courses"`
}

type AddedDepartmentCourses struct {
	AddedDepartmentCourses []AddedDepartmentCourse `json:"added_department_courses"`
}

type AddedOtherElectiveCourses struct {
	AddedOtherElectiveCourses []AddedOtherElectiveCourse `json:"added_other_elective_courses"`
}

type AddedCommonElectiveCourses struct {
	AddedCommonElectiveCourses []AddedCommonElectiveCourse `json:"added_common_elective_courses"`
}
