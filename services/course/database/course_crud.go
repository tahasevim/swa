package database

import (
	"fmt"

	"github.com/nursena/SWA/services/course/models"
)

// FindAll returns the list of the courses
func FindAll() ([]*models.Course, error) {
	//var courses []*models.Course
	return nil, nil

}

// Insert a student into database
func Insert(student models.Course) error {
	return nil

}

// Delete the collection
func DeleteAllCourses() error {
	// rows, err := DB.Exec()
	return nil
}

func GetDepartmentMustCourses(departmentCourseIDs []string) ([]models.ShownDepartmentCourse, error) {
	// rows, err := DB.Table(departmentCoursesTableName).Joins("left join courses on courses.course_id = department_courses.course_id").Rows()
	// for rows.Next() {
	// 	fmt.Println(rows)
	// }

	var shownDepartmentCourses []models.ShownDepartmentCourse

	finalColumnNames := "department_courses.department_course_id, courses.course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, department_courses.is_must, department_courses.term"
	// DB.Table(departmentCoursesTableName).Select(finalColumnNames).
	// 	Joins("join courses on courses.course_id = department_courses.course_id").Where("department_course_id in (?)", departmentCourseIDs).Scan(&coursesToBeShown)
	err := DB.Table(coursesTableName).Select(finalColumnNames).
		Joins("join department_courses on courses.course_id = department_courses.course_id").Where("department_courses.department_course_id IN (?)", departmentCourseIDs).Scan(&shownDepartmentCourses).Error

	fmt.Println(shownDepartmentCourses)

	return shownDepartmentCourses, err
}

func GetDepartmentOtherElectiveCourses(otherElectiveCourseIDs []string) ([]models.ShownOtherElectiveCourse, error) {
	var shownOtherElectiveCourses []models.ShownOtherElectiveCourse

	finalColumnNames := "other_elective_courses.other_elective_course_id, courses.course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, other_elective_courses.term"
	err := DB.Table(coursesTableName).Select(finalColumnNames).
		Joins("join other_elective_courses on courses.course_id = other_elective_courses.course_id").Where("other_elective_courses.other_elective_course_id IN (?)", otherElectiveCourseIDs).Scan(&shownOtherElectiveCourses).Error

	fmt.Println(shownOtherElectiveCourses)

	return shownOtherElectiveCourses, err
}

func GetCommonElectiveCourses() ([]models.Course, error) {
	var shownCommonElectiveCourses []models.Course

	err := DB.Table(coursesTableName).Where("is_common_elective = ?", true).Scan(&shownCommonElectiveCourses).Error

	fmt.Println("commonElectiveCourses", shownCommonElectiveCourses)

	return shownCommonElectiveCourses, err
}

func GetDepartmentInfo(departmentID string) (models.Department, error) {
	var department models.Department
	err := DB.Table(departmentsTableName).Where("department_id = ?", departmentID).First(&department).Error

	return department, err

}

func GetAdvisorInfo(teacherID string) (models.Teacher, error) {
	var teacher models.Teacher
	err := DB.Table(teachersTableName).Where("teacher_id = ?", teacherID).First(&teacher).Error

	return teacher, err

}

func GetSectionIDs(courseID string) models.SectionRetrieval {
	var sectionIDs models.SectionRetrieval
	err := DB.Table(coursesTableName).Select("courses.section_ids").Where("course_id = ?", courseID).First(&sectionIDs).Error
	fmt.Println(err)
	return sectionIDs
}

func GetCourseSectionsData(sectionIDs []string) ([]models.ShownCourseSection, error) {
	var shownCourseSections []models.ShownCourseSection

	finalColumnNames := "sections.section_id, sections.quota, sections.remaining_quota, sections.section, teachers.name, teachers.surname, teachers.academic_rank"
	err := DB.
		Table(sectionsTableName).Select(finalColumnNames).
		Joins("join teachers on sections.teacher_id = teachers.teacher_id").
		Where("sections.section_id in (?)", sectionIDs).
		Scan(&shownCourseSections).Error

	fmt.Println("courseSections", shownCourseSections)
	return shownCourseSections, err

}
