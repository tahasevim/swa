package database

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	"github.com/nursena/SWA/services/course/models"

	_ "github.com/lib/pq"
	"github.com/nursena/SWA/services/course/database/config"
)

var DB *gorm.DB

var dao = DAO{}
var studentsTableName = "students"
var teachersTableName = "teachers"
var coursesTableName = "courses"
var sectionsTableName = "sections"
var departmentCoursesTableName = "department_courses"
var otherElectiveCoursesTableName = "other_elective_courses"
var departmentsTableName = "departments"
var takenDepartmentCoursesTableName = "taken_department_courses"
var takenOtherElectiveCoursesTableName = "taken_other_elective_courses"
var takenCommonElectiveCoursesTableName = "taken_common_elective_courses"
var addedDepartmentCoursesTableName = "added_department_courses"
var addedOtherElectiveCoursesTableName = "added_other_elective_courses"
var addedCommonElectiveCoursesTableName = "added_common_elective_courses"

type DAO struct {
	DatabaseURI  string
	DatabaseName string
}

func ClearDatabase() {
	fmt.Println("Clearing database...")

	DB.DropTable(&models.Course{})
	DB.DropTable(&models.Section{})
	DB.DropTable(&models.DepartmentCourse{})
	DB.DropTable(&models.OtherElectiveCourse{})
	DB.DropTable(&models.Department{})
	DB.DropTable(&models.TakenDepartmentCourse{})
	DB.DropTable(&models.TakenOtherElectiveCourse{})
	DB.DropTable(&models.TakenCommonElectiveCourse{})

	DB.DropTable(&models.AddedDepartmentCourse{})
	DB.DropTable(&models.AddedOtherElectiveCourse{})
	DB.DropTable(&models.AddedCommonElectiveCourse{})

	fmt.Println("Tables are dropped.")

	// if !DB.HasTable(coursesTableName) { // ?????
	//fmt.Println("Doesn't exist courses table.")

	DB.Table(coursesTableName).CreateTable(&models.Course{})
	DB.Table(sectionsTableName).CreateTable(&models.Section{})
	DB.Table(departmentCoursesTableName).CreateTable(&models.DepartmentCourse{})
	DB.Table(otherElectiveCoursesTableName).CreateTable(&models.OtherElectiveCourse{})
	DB.Table(departmentsTableName).CreateTable(&models.Department{})
	DB.Table(takenDepartmentCoursesTableName).CreateTable(&models.TakenDepartmentCourse{})
	DB.Table(takenOtherElectiveCoursesTableName).CreateTable(&models.TakenOtherElectiveCourse{})
	DB.Table(takenCommonElectiveCoursesTableName).CreateTable(&models.TakenCommonElectiveCourse{})

	DB.Table(addedDepartmentCoursesTableName).CreateTable(&models.AddedDepartmentCourse{})
	DB.Table(addedOtherElectiveCoursesTableName).CreateTable(&models.AddedOtherElectiveCourse{})
	DB.Table(addedCommonElectiveCoursesTableName).CreateTable(&models.AddedCommonElectiveCourse{})

	fmt.Println("Tables are re-created.")
	// }

	fmt.Println("Database is cleared.")
}

// //InitDatabase initializes the database with predefined data
func InitDatabase(resetDB bool) {
	jsonFile, err := os.Open("database/course-pool/courses.json")
	defer jsonFile.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var courses models.Courses
	json.Unmarshal(byteValue, &courses)

	var sections models.Sections
	json.Unmarshal(byteValue, &sections)

	var departmentCourses models.DepartmentCourses
	json.Unmarshal(byteValue, &departmentCourses)

	var otherElectiveCourses models.OtherElectiveCourses
	json.Unmarshal(byteValue, &otherElectiveCourses)

	var departments models.Departments
	json.Unmarshal(byteValue, &departments)

	var takenDepartmentCourses models.TakenDepartmentCourses
	json.Unmarshal(byteValue, &takenDepartmentCourses)

	var takenOtherElectiveCourses models.TakenOtherElectiveCourses
	json.Unmarshal(byteValue, &takenOtherElectiveCourses)

	var takenCommonElectiveCourses models.TakenCommonElectiveCourses
	json.Unmarshal(byteValue, &takenCommonElectiveCourses)

	var addedDepartmentCourses models.AddedDepartmentCourses
	json.Unmarshal(byteValue, &addedDepartmentCourses)

	var addedOtherElectiveCourses models.AddedOtherElectiveCourses
	json.Unmarshal(byteValue, &addedOtherElectiveCourses)

	var addedCommonElectiveCourses models.AddedCommonElectiveCourses
	json.Unmarshal(byteValue, &addedCommonElectiveCourses)

	if resetDB {
		ClearDatabase()
	}

	fmt.Println("Adding courses...")
	for i := 0; i < len(courses.Courses); i++ {
		c := models.Course{
			CourseID:         courses.Courses[i].CourseID,
			CourseCode:       courses.Courses[i].CourseCode,
			OpticCode:        courses.Courses[i].OpticCode,
			CourseName:       courses.Courses[i].CourseName,
			TheoryHour:       courses.Courses[i].TheoryHour,
			PraticsHour:      courses.Courses[i].PraticsHour,
			Credit:           courses.Courses[i].Credit,
			Akts:             courses.Courses[i].Akts,
			IsCommonElective: courses.Courses[i].IsCommonElective,
			SectionIDs:       courses.Courses[i].SectionIDs,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(sections.Sections); i++ {
		c := models.Section{
			SectionID:      sections.Sections[i].SectionID,
			Quota:          sections.Sections[i].Quota,
			RemainingQuota: sections.Sections[i].RemainingQuota,
			Section:        sections.Sections[i].Section,
			TeacherID:      sections.Sections[i].TeacherID,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(departmentCourses.DepartmentCourses); i++ {
		c := models.DepartmentCourse{
			DepartmentCourseID: departmentCourses.DepartmentCourses[i].DepartmentCourseID,
			CourseID:           departmentCourses.DepartmentCourses[i].CourseID,
			IsMust:             departmentCourses.DepartmentCourses[i].IsMust,
			Term:               departmentCourses.DepartmentCourses[i].Term,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(otherElectiveCourses.OtherElectiveCourses); i++ {
		c := models.OtherElectiveCourse{
			OtherElectiveCourseID: otherElectiveCourses.OtherElectiveCourses[i].OtherElectiveCourseID,
			CourseID:              otherElectiveCourses.OtherElectiveCourses[i].CourseID,
			Term:                  otherElectiveCourses.OtherElectiveCourses[i].Term,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(departments.Departments); i++ {
		c := models.Department{
			DepartmentID:           departments.Departments[i].DepartmentID,
			DepartmentCode:         departments.Departments[i].DepartmentCode,
			DepartmentName:         departments.Departments[i].DepartmentName,
			FacultyName:            departments.Departments[i].FacultyName,
			DepartmentCourseIDs:    departments.Departments[i].DepartmentCourseIDs,
			OtherElectiveCourseIDs: departments.Departments[i].OtherElectiveCourseIDs,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(takenDepartmentCourses.TakenDepartmentCourses); i++ {
		c := models.TakenDepartmentCourse{
			TakenDepartmentCourseID: takenDepartmentCourses.TakenDepartmentCourses[i].TakenDepartmentCourseID,
			DepartmentCourseID:      takenDepartmentCourses.TakenDepartmentCourses[i].DepartmentCourseID,
			FinalGrade:              takenDepartmentCourses.TakenDepartmentCourses[i].FinalGrade,
			MakeUpGrade:             takenDepartmentCourses.TakenDepartmentCourses[i].MakeUpGrade,
			IsPassed:                takenDepartmentCourses.TakenDepartmentCourses[i].IsPassed,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(takenOtherElectiveCourses.TakenOtherElectiveCourses); i++ {
		c := models.TakenOtherElectiveCourse{
			TakenOtherElectiveCourseID: takenOtherElectiveCourses.TakenOtherElectiveCourses[i].TakenOtherElectiveCourseID,
			OtherElectiveCourseID:      takenOtherElectiveCourses.TakenOtherElectiveCourses[i].OtherElectiveCourseID,
			FinalGrade:                 takenOtherElectiveCourses.TakenOtherElectiveCourses[i].FinalGrade,
			MakeUpGrade:                takenOtherElectiveCourses.TakenOtherElectiveCourses[i].MakeUpGrade,
			IsPassed:                   takenOtherElectiveCourses.TakenOtherElectiveCourses[i].IsPassed,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(takenCommonElectiveCourses.TakenCommonElectiveCourses); i++ {
		c := models.TakenCommonElectiveCourse{
			TakenCommonElectiveCourseID: takenCommonElectiveCourses.TakenCommonElectiveCourses[i].TakenCommonElectiveCourseID,
			CourseID:                    takenCommonElectiveCourses.TakenCommonElectiveCourses[i].CourseID,
			FinalGrade:                  takenCommonElectiveCourses.TakenCommonElectiveCourses[i].FinalGrade,
			MakeUpGrade:                 takenCommonElectiveCourses.TakenCommonElectiveCourses[i].MakeUpGrade,
			IsPassed:                    takenCommonElectiveCourses.TakenCommonElectiveCourses[i].IsPassed,
			Term:                        takenCommonElectiveCourses.TakenCommonElectiveCourses[i].Term,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(addedDepartmentCourses.AddedDepartmentCourses); i++ {
		c := models.AddedDepartmentCourse{
			AddedDepartmentCourseID: addedDepartmentCourses.AddedDepartmentCourses[i].AddedDepartmentCourseID,
			DepartmentCourseID:      addedDepartmentCourses.AddedDepartmentCourses[i].DepartmentCourseID,
			Section:                 addedDepartmentCourses.AddedDepartmentCourses[i].Section,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(addedOtherElectiveCourses.AddedOtherElectiveCourses); i++ {
		c := models.AddedOtherElectiveCourse{
			AddedOtherElectiveCourseID: addedOtherElectiveCourses.AddedOtherElectiveCourses[i].AddedOtherElectiveCourseID,
			OtherElectiveCourseID:      addedOtherElectiveCourses.AddedOtherElectiveCourses[i].OtherElectiveCourseID,
			Section:                    addedOtherElectiveCourses.AddedOtherElectiveCourses[i].Section,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(addedCommonElectiveCourses.AddedCommonElectiveCourses); i++ {
		c := models.AddedCommonElectiveCourse{
			AddedCommonElectiveCourseID: addedCommonElectiveCourses.AddedCommonElectiveCourses[i].AddedCommonElectiveCourseID,
			CourseID:                    addedCommonElectiveCourses.AddedCommonElectiveCourses[i].CourseID,
			Section:                     addedCommonElectiveCourses.AddedCommonElectiveCourses[i].Section,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	fmt.Println("Database is initialized.")
}

// Establish a connection to database
func (d *DAO) Connect() {
	connStr := os.Getenv("POSTGREDB_URI")

	db, err := gorm.Open("postgres", connStr)

	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()

	DB = db
	err = db.DB().Ping()

	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Connection to PostgreSQL established.")
}

// Parse the configuration file 'config.toml', and establish a connection to DB
func ConnectDB() {
	initDB := false
	resetDB := true

	config := config.Config{}

	config.Read()

	dao.DatabaseURI = config.DatabaseURI
	dao.DatabaseName = config.DatabaseName
	dao.Connect()

	if initDB {
		InitDatabase(resetDB)
	}

}
