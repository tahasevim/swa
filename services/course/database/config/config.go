package config

// Represents database credentials
type Config struct {
	DatabaseURI  string
	DatabaseName string
}

// Read and parse the configuration file
func (c *Config) Read() {
	// if _, err := toml.DecodeFile("config.toml", &c); err != nil {
	// 	log.Fatal(err)
	// }

	c.DatabaseName = "CourseRegistration"
	c.DatabaseURI = ""

	// c.DatabaseURI = os.Getenv("POSTGREDB_URI")
	// c.DatabaseName = os.Getenv("POSTGREDB_NAME")

	// if c.DatabaseURI == "" {
	// 	log.Fatal(errors.New("Empty DatabaseURI"))
	// }

	// if c.DatabaseURI == "" {
	// 	log.Fatal(errors.New("Empty DatabaseName"))
	// }

}
