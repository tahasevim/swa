package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/nursena/SWA/services/course/controller"
	"github.com/nursena/SWA/services/course/database"
)

func main() {
	r := mux.NewRouter()
	database.ConnectDB()
	r.HandleFunc("/api/department-courses", controller.GetDepartmentCourses).Methods("GET")
	r.HandleFunc("/api/elective-courses", controller.GetElectiveCourses).Methods("GET")
	r.HandleFunc("/api/department", controller.GetDepartmentAndAdvisorInformation).Methods("GET")
	r.HandleFunc("/api/course-sections", controller.GetCourseSections).Methods("GET")

	if err := http.ListenAndServe(":8080", r); err != nil {
		log.Fatal(err)
	}
}
