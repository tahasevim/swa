package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/nursena/SWA/services/user/controller"
	"github.com/nursena/SWA/services/user/database"
)

func main() {
	r := mux.NewRouter()
	database.ConnectDB()
	r.HandleFunc("/api/user/added-courses", controller.GetAddedStudentCourses).Methods("GET")
	r.HandleFunc("/api/user/taken-courses", controller.GetTakenStudentCourses).Methods("GET")
	r.HandleFunc("/api/user/account/password", controller.UpdateAccountPassword).Methods("PUT")
	r.HandleFunc("/api/user/account/email", controller.UpdateAccountEmail).Methods("PUT")
	r.HandleFunc("/api/user/complete-course-registration", controller.CompleteCourseRegistration).Methods("PUT")
	r.HandleFunc("/api/user/add-course", controller.AddCourse).Methods("POST")
	r.HandleFunc("/api/user/remove-course", controller.RemoveCourse).Methods("PUT")
	if err := http.ListenAndServe(":8080", r); err != nil {
		log.Fatal(err)
	}
}
