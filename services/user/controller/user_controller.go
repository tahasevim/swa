package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/nursena/SWA/services/user/database"
	"github.com/nursena/SWA/services/user/models"
	"golang.org/x/crypto/bcrypt"
)

func AddCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Adding course")
	defer r.Body.Close()
	var addingCourse models.AddingCourse
	if err := json.NewDecoder(r.Body).Decode(&addingCourse); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	remainingAkts := database.GetRemainingAkts(addingCourse.StudentID)
	fmt.Println(remainingAkts)
	if remainingAkts.RemainingAkts < addingCourse.Akts {
		respondWithError(w, http.StatusNotAcceptable, "There is no remaining akts.")
		return
	}
	remainingCourseQuota := database.GetSectionQuota(addingCourse.SectionID)
	fmt.Println(remainingCourseQuota)
	if remainingCourseQuota.RemainingQuota == 0 {
		respondWithError(w, http.StatusForbidden, "The remaining quota of the selected section is zero.")
		return
	}
	// Decrement remaining quota by 1
	database.SetPlaceFromCourse(addingCourse.SectionID)
	database.SetPlaceAktsFromStudent(addingCourse.StudentID, addingCourse.Akts)

	err, addedCourseUUID := database.AddCourse(addingCourse.StudentID, addingCourse.AddedCourseType, addingCourse.AddedCourseID, addingCourse.Section)

	if err != nil {
		database.RevertSetPlaceFromCourse(addingCourse.SectionID)
		database.RevertSetPlaceAktsFromStudent(addingCourse.StudentID, addingCourse.Akts)
		respondWithError(w, http.StatusGone, "Some error is occurred, failed.")
		return
	}

	respondWithJSON(w, http.StatusCreated, map[string]string{"addedCourseUUID": addedCourseUUID})

}

func RemoveCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Removing course")
	defer r.Body.Close()
	var removingCourse models.RemovingCourse
	if err := json.NewDecoder(r.Body).Decode(&removingCourse); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	fmt.Println(removingCourse)
	database.IncrementStudentRemainingAkts(removingCourse.StudentID, removingCourse.Akts)
	database.IncrementCourseSectionQuota(removingCourse.SectionID)
	err := database.RemoveCourseIDFromStudentTable(removingCourse.StudentID, removingCourse.RemovedCourseID, removingCourse.RemovedCourseType)

	if err != nil {
		respondWithError(w, http.StatusGone, "Some error is occurred, failed.")
		return
	}

	respondWithJSON(w, http.StatusCreated, nil)

}

func UpdateAccountPassword(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Updating Account Password")
	defer r.Body.Close()
	var student models.StudentAccountUpdateStruct
	if err := json.NewDecoder(r.Body).Decode(&student); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	dbUser, err := database.FindByID(student.ID) // Has hashed password
	if err != nil {
		fmt.Println("There is no such user with the specified student number in the database.")
		respondWithError(w, http.StatusNotFound, err.Error())
		return
	}

	byteDBHashedPassword := []byte(dbUser.Password)
	bytePlainTextOldPassword := []byte(student.OldPassword)

	err = bcrypt.CompareHashAndPassword(byteDBHashedPassword, bytePlainTextOldPassword)

	if err != nil {
		fmt.Println("The old password doesn't match with the one in the database.")
		respondWithError(w, http.StatusUnauthorized, err.Error())
		return
	}

	fmt.Println("The old password is true.")

	byteNewPassword := []byte(student.NewPassword)
	hashedPassword, err := bcrypt.GenerateFromPassword(byteNewPassword, 2) // n cost means 2^n rounds, should be determined according to benchmarks
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	student.NewPassword = string(hashedPassword)
	if err := database.UpdatePassword(student); err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

func UpdateAccountEmail(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Updating Account Email")
	defer r.Body.Close()
	var student models.StudentAccountUpdateStruct
	if err := json.NewDecoder(r.Body).Decode(&student); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := database.UpdateEmail(student); err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

func CompleteCourseRegistration(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Completing Course Registration")
	defer r.Body.Close()

	var finalizeCourseRegistrationStruct models.FinalizeCourseRegistrationStruct
	if err := json.NewDecoder(r.Body).Decode(&finalizeCourseRegistrationStruct); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
	}

	err := database.DisableCourseRegistration(finalizeCourseRegistrationStruct.StudentID)

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, nil)
}

func GetAddedStudentCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting Added Student Courses")
	studentID := r.Header.Get("studentID")
	defer r.Body.Close()
	dbUser, err := database.FindByID(studentID)
	fmt.Println("GetAddedStudentCourses, user: ", dbUser)

	shownAddedDepartmentCourses, err := database.GetAddedDepartmentCourses(dbUser.AddedDepartmentCourseIDs)
	shownAddedOtherElectiveCourses, err := database.GetAddedOtherElectiveCourses(dbUser.AddedOtherElectiveCourseIDs)
	shownAddedCommonElectiveCourses, err := database.GetCommonElectiveCourses(dbUser.AddedCommonElectiveCourseIDs)

	var obj models.AddedStudentCoursesReturn
	obj.ShownAddedDepartmentCourse = shownAddedDepartmentCourses
	obj.ShownAddedOtherElectiveCourse = shownAddedOtherElectiveCourses
	obj.ShownAddedCommonElectiveCourse = shownAddedCommonElectiveCourses

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, obj)
}

func GetTakenStudentCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting Taken Student Courses")
	studentID := r.Header.Get("studentID")
	defer r.Body.Close()
	dbUser, err := database.FindByID(studentID)
	fmt.Println("GetTakenStudentCourses, user: ", dbUser)

	shownTakenDepartmentCourses, err := database.GetTakenDepartmentCourses(dbUser.TakenDepartmentCourseIDs)
	shownTakenOtherElectiveCourses, err := database.GetTakenOtherElectiveCourses(dbUser.TakenOtherElectiveCourseIDs)
	shownTakenCommonElectiveCourses, err := database.GetTakenCommonElectiveCourses(dbUser.TakenCommonElectiveCourseIDs)

	var obj models.TakenStudentCoursesReturn
	obj.ShownTakenDepartmentCourse = shownTakenDepartmentCourses
	obj.ShownTakenOtherElectiveCourse = shownTakenOtherElectiveCourses
	obj.ShownTakenCommonElectiveCourse = shownTakenCommonElectiveCourses

	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, obj)
}
