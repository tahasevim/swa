package database

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	"github.com/nursena/SWA/services/user/models"

	"golang.org/x/crypto/bcrypt"

	_ "github.com/lib/pq"
	"github.com/nursena/SWA/services/user/database/config"
)

var DB *gorm.DB

var dao = DAO{}

var coursesTableName = "courses"
var departmentCoursesTableName = "department_courses"

var studentsTableName = "students"
var teachersTableName = "teachers"

type DAO struct {
	DatabaseURI  string
	DatabaseName string
}

func ClearDatabase() {
	fmt.Println("Clearing database...")

	DB.DropTable(&models.Student{})
	DB.DropTable(&models.Teacher{})

	fmt.Println("Tables are dropped.")

	// if !DB.HasTable(studentsTableName) { // ?????
	//fmt.Println("Doesn't exist courses table.")

	DB.Table(studentsTableName).CreateTable(&models.Student{})
	DB.Table(teachersTableName).CreateTable(&models.Teacher{})

	fmt.Println("Tables are re-created.")
	// }

	fmt.Println("Database is cleared.")
}

// //InitDatabase initializes the database with predefined data
func InitDatabase(resetDB bool) {
	jsonFile, err := os.Open("database/student-pool/student.json")
	defer jsonFile.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var students models.Students
	json.Unmarshal(byteValue, &students)

	var teachers models.Teachers
	json.Unmarshal(byteValue, &teachers)

	if resetDB {
		ClearDatabase()
	}

	fmt.Println("Adding students...")
	for i := 0; i < len(students.Students); i++ {
		bytePassword := []byte(students.Students[i].Password)
		hashedPassword, _ := bcrypt.GenerateFromPassword(bytePassword, 2)
		password := string(hashedPassword)
		c := models.Student{
			StudentID:                    students.Students[i].StudentID,
			StudentNumber:                students.Students[i].StudentNumber,
			Name:                         students.Students[i].Name,
			Surname:                      students.Students[i].Surname,
			Email:                        students.Students[i].Email,
			Password:                     password,
			Year:                         students.Students[i].Year,
			Term:                         students.Students[i].Term,
			SubjectedCreditType:          students.Students[i].SubjectedCreditType,
			DepartmentID:                 students.Students[i].DepartmentID,
			AdvisorID:                    students.Students[i].AdvisorID,
			AddedDepartmentCourseIDs:     students.Students[i].AddedDepartmentCourseIDs,
			AddedOtherElectiveCourseIDs:  students.Students[i].AddedOtherElectiveCourseIDs,
			AddedCommonElectiveCourseIDs: students.Students[i].AddedCommonElectiveCourseIDs,
			TakenDepartmentCourseIDs:     students.Students[i].TakenDepartmentCourseIDs,
			TakenOtherElectiveCourseIDs:  students.Students[i].TakenOtherElectiveCourseIDs,
			TakenCommonElectiveCourseIDs: students.Students[i].TakenCommonElectiveCourseIDs,
			CourseRegistrationConfirmed:  students.Students[i].CourseRegistrationConfirmed,
			CourseRegistrationEnabled:    students.Students[i].CourseRegistrationEnabled,
			RemainingAkts:                students.Students[i].RemainingAkts,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	for i := 0; i < len(teachers.Teachers); i++ {
		c := models.Teacher{
			TeacherID:    teachers.Teachers[i].TeacherID,
			Name:         teachers.Teachers[i].Name,
			Surname:      teachers.Teachers[i].Surname,
			AcademicRank: teachers.Teachers[i].AcademicRank,
		}
		fmt.Println(c)
		DB.Create(c)
	}

	fmt.Println("Database is initialized.")
}

// Establish a connection to database
func (d *DAO) Connect() {
	connStr := os.Getenv("POSTGREDB_URI")

	db, err := gorm.Open("postgres", connStr)

	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()

	DB = db
	err = db.DB().Ping()

	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Connection to PostgreSQL established.")
}

// Parse the configuration file 'config.toml', and establish a connection to DB
func ConnectDB() {
	initDB := true
	resetDB := true

	config := config.Config{}

	config.Read()

	dao.DatabaseURI = config.DatabaseURI
	dao.DatabaseName = config.DatabaseName
	dao.Connect()

	if initDB {
		InitDatabase(resetDB)
	}

}
