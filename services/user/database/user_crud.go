package database

import (
	"fmt"

	"github.com/lib/pq"

	uuid "github.com/satori/go.uuid"

	"github.com/jinzhu/gorm"
	"github.com/nursena/SWA/services/user/models"
)

// Update an existing student's password
func UpdatePassword(student models.StudentAccountUpdateStruct) error {
	err := DB.Table(studentsTableName).Where("student_id = ?", student.ID).Update("password", student.NewPassword).Error
	return err
}

// Update an existing student
func UpdateEmail(student models.StudentAccountUpdateStruct) error {
	err := DB.Table(studentsTableName).Where("student_id = ?", student.ID).Update("email", student.Email).Error
	return err
}

// FindByID returns the student information with the specified ID
func FindByID(id string) (models.Student, error) {
	var student models.Student
	err := DB.Table(studentsTableName).Where("student_id = ?", id).First(&student).Error

	return student, err
}

func GetTakenDepartmentCourses(takenDepartmentCourseIDs []string) ([]models.ShownTakenDepartmentCourse, error) {
	var shownTakenDepartmentCourses []models.ShownTakenDepartmentCourse

	finalColumnNames := "taken_department_courses.taken_department_course_id, department_courses.department_course_id, courses.course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, department_courses.is_must, department_courses.term, taken_department_courses.final_grade, taken_department_courses.make_up_grade, taken_department_courses.is_passed"

	fmt.Println(takenDepartmentCourseIDs)
	err := DB.
		Table(coursesTableName).Select(finalColumnNames).
		Joins("join department_courses on courses.course_id = department_courses.course_id").
		Joins("join taken_department_courses on department_courses.department_course_id = taken_department_courses.department_course_id").
		Where("taken_department_courses.taken_department_course_id in (?)", takenDepartmentCourseIDs).
		Scan(&shownTakenDepartmentCourses).Error

	fmt.Println("takenDepartmentCourses", shownTakenDepartmentCourses)
	return shownTakenDepartmentCourses, err
}

func GetTakenOtherElectiveCourses(takenOtherElectiveCourseIDs []string) ([]models.ShownTakenOtherElectiveCourse, error) {
	var shownTakenOtherElectiveCourses []models.ShownTakenOtherElectiveCourse

	finalColumnNames := "taken_other_elective_courses.taken_other_elective_course_id, other_elective_courses.other_elective_course_id, courses.course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, other_elective_courses.term, taken_other_elective_courses.final_grade, taken_other_elective_courses.make_up_grade, taken_other_elective_courses.is_passed"

	fmt.Println(takenOtherElectiveCourseIDs)
	err := DB.
		Table(coursesTableName).Select(finalColumnNames).
		Joins("join other_elective_courses on courses.course_id = other_elective_courses.course_id").
		Joins("join taken_other_elective_courses on other_elective_courses.other_elective_course_id = taken_other_elective_courses.other_elective_course_id").
		Where("taken_other_elective_courses.taken_other_elective_course_id in (?)", takenOtherElectiveCourseIDs).
		Scan(&shownTakenOtherElectiveCourses).Error

	fmt.Println("takenOtherElectiveCourses", shownTakenOtherElectiveCourses)
	return shownTakenOtherElectiveCourses, err
}

func GetTakenCommonElectiveCourses(takenCommonElectiveCourseIDs []string) ([]models.ShownTakenCommonElectiveCourse, error) {
	var shownTakenCommonElectiveCourses []models.ShownTakenCommonElectiveCourse

	finalColumnNames := "taken_common_elective_courses.taken_common_elective_course_id, courses.course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, taken_common_elective_courses.term, taken_common_elective_courses.final_grade, taken_common_elective_courses.make_up_grade, taken_common_elective_courses.is_passed"

	fmt.Println(takenCommonElectiveCourseIDs)
	err := DB.
		Table(coursesTableName).Select(finalColumnNames).
		Joins("join taken_common_elective_courses on courses.course_id = taken_common_elective_courses.course_id").
		Where("taken_common_elective_courses.taken_common_elective_course_id in (?)", takenCommonElectiveCourseIDs).
		Scan(&shownTakenCommonElectiveCourses).Error

	fmt.Println("takenCommonElectiveCourses", shownTakenCommonElectiveCourses)
	return shownTakenCommonElectiveCourses, err
}

func GetAddedDepartmentCourses(addedDepartmentCourseIDs []string) ([]models.ShownAddedDepartmentCourse, error) {
	var shownDepartmentCourses []models.ShownAddedDepartmentCourse

	finalColumnNames := "added_department_courses.added_department_course_id, courses.course_id, department_courses.department_course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, department_courses.is_must, department_courses.term, added_department_courses.section"
	err := DB.
		Table(coursesTableName).Select(finalColumnNames).
		Joins("join department_courses on courses.course_id = department_courses.course_id").
		Joins("join added_department_courses on department_courses.department_course_id = added_department_courses.department_course_id").
		Where("added_department_courses.added_department_course_id in (?)", addedDepartmentCourseIDs).
		Scan(&shownDepartmentCourses).Error

	fmt.Println("addedDepartmentCourses", shownDepartmentCourses)
	return shownDepartmentCourses, err
}

func GetAddedOtherElectiveCourses(addedOtherElectiveCourseIDs []string) ([]models.ShownAddedOtherElectiveCourse, error) {
	var shownOtherElectiveCourses []models.ShownAddedOtherElectiveCourse
	finalColumnNames := "added_other_elective_courses.added_other_elective_course_id, courses.course_id, other_elective_courses.other_elective_course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, other_elective_courses.term, added_other_elective_courses.section"
	err := DB.
		Table(coursesTableName).Select(finalColumnNames).
		Joins("join other_elective_courses on courses.course_id = other_elective_courses.course_id").
		Joins("join added_other_elective_courses on other_elective_courses.other_elective_course_id = added_other_elective_courses.other_elective_course_id").
		Where("added_other_elective_courses.added_other_elective_course_id in (?)", addedOtherElectiveCourseIDs).
		Scan(&shownOtherElectiveCourses).Error

	fmt.Println("addedOtherElectiveCourses", shownOtherElectiveCourses)
	return shownOtherElectiveCourses, err
}

func GetCommonElectiveCourses(addedCommonElectiveCourseIDs []string) ([]models.ShownAddedCommonElectiveCourse, error) {
	var shownCommonElectiveCourses []models.ShownAddedCommonElectiveCourse
	finalColumnNames := "added_common_elective_courses.added_common_elective_course_id, courses.course_id, courses.course_code, courses.optic_code, courses.course_name, courses.theory_hour, courses.pratics_hour, courses.credit, courses.akts, courses.section_ids, added_common_elective_courses.section"

	err := DB.
		Table(coursesTableName).Select(finalColumnNames).
		Joins("join added_common_elective_courses on courses.course_id = added_common_elective_courses.course_id").
		Where("added_common_elective_courses.added_common_elective_course_id in (?)", addedCommonElectiveCourseIDs).
		Scan(&shownCommonElectiveCourses).Error
	fmt.Println("addedCommonElectiveCourses", shownCommonElectiveCourses)

	return shownCommonElectiveCourses, err
}

func GetRemainingAkts(studentID string) models.RemainingAktsQuery {
	var remainingAkts models.RemainingAktsQuery
	DB.Table(studentsTableName).Select("students.remaining_akts").Where("students.student_id = ?", studentID).First(&remainingAkts)
	return remainingAkts
}

func GetSectionQuota(sectionID string) models.RemainingQuotaQuery {
	var remainingQuota models.RemainingQuotaQuery
	DB.Table("sections").Select("sections.remaining_quota").Where("sections.section_id = ?", sectionID).First(&remainingQuota)
	return remainingQuota
}

func SetPlaceFromCourse(sectionID string) {
	var section models.Section
	DB.Model(&section).Where("section_id = ?", sectionID).UpdateColumn("remaining_quota", gorm.Expr("remaining_quota - ?", 1))
}

func RevertSetPlaceFromCourse(sectionID string) {
	var section models.Section
	DB.Model(&section).Where("section_id = ?", sectionID).UpdateColumn("remaining_quota", gorm.Expr("remaining_quota + ?", 1))
}

func IncrementCourseSectionQuota(sectionID string) {
	var section models.Section
	DB.Model(&section).Where("section_id = ?", sectionID).UpdateColumn("remaining_quota", gorm.Expr("remaining_quota + ?", 1))
}

func IncrementStudentRemainingAkts(studentID string, akts int) {
	var student models.Student
	DB.Model(&student).Where("student_id = ?", studentID).UpdateColumn("remaining_akts", gorm.Expr("remaining_akts + ?", akts))
}

func SetPlaceAktsFromStudent(studentID string, akts int) {
	var student models.Student
	DB.Model(&student).Where("student_id = ?", studentID).UpdateColumn("remaining_akts", gorm.Expr("remaining_akts - ?", akts))
}

func RevertSetPlaceAktsFromStudent(studentID string, akts int) {
	var student models.Student
	DB.Model(&student).Where("student_id = ?", studentID).UpdateColumn("remaining_akts", gorm.Expr("remaining_akts + ?", akts))
}

func remove(items []string, item string) pq.StringArray {
	var newitems []string
	for _, i := range items {
		if i != item {
			fmt.Println("**")
			fmt.Println(i)
			fmt.Println("not equal to")
			fmt.Println(item)
			newitems = append(newitems, i)
		}
	}

	return newitems
}

func RemoveCourseIDFromStudentTable(studentID string, removedCourseID string, courseType string) error {
	var student models.Student

	var addedDepartmentCourses models.AddedDepartmentCourses
	var addedOtherElectiveCourses models.AddedOtherElectiveCourses
	var addedCommonElectiveCourses models.AddedCommonElectiveCourses

	var err error

	if courseType == "departmentCourse" {
		fmt.Println("departmentCourse")
		err = DB.Table("students").Select("students.added_department_course_ids").Where("students.student_id = ?", studentID).First(&addedDepartmentCourses).Error
		fmt.Println("before", addedDepartmentCourses.AddedDepartmentCourseIDs)
		finalArray := remove(addedDepartmentCourses.AddedDepartmentCourseIDs, removedCourseID)
		fmt.Println("after", finalArray)
		err = DB.Model(&student).Update("added_department_course_ids", finalArray).Error
	} else if courseType == "otherElectiveCourse" {
		err = DB.Table("students").Select("students.added_other_elective_course_ids").Where("students.student_id = ?", studentID).First(&addedOtherElectiveCourses).Error
		finalArray := remove(addedOtherElectiveCourses.AddedOtherElectiveCourseIDs, removedCourseID)
		err = DB.Model(&student).Update("added_other_elective_course_ids", finalArray).Error
	} else if courseType == "commonElectiveCourse" {
		err = DB.Table("students").Select("students.added_common_elective_course_ids").Where("students.student_id = ?", studentID).First(&addedCommonElectiveCourses).Error
		finalArray := remove(addedCommonElectiveCourses.AddedCommonElectiveCourseIDs, removedCourseID)
		err = DB.Model(&student).Update("added_common_elective_course_ids", finalArray).Error
	}

	return err

}

func AddCourse(studentID string, addedCourseType string, addedCourseID string, section int) (error, string) {
	var student models.Student

	var addedDepartmentCourse models.AddedDepartmentCourse
	var addedOtherElectiveCourse models.AddedOtherElectiveCourse
	var addedCommonElectiveCourse models.AddedCommonElectiveCourse

	var addedDepartmentCourses models.AddedDepartmentCourses
	var addedOtherElectiveCourses models.AddedOtherElectiveCourses
	var addedCommonElectiveCourses models.AddedCommonElectiveCourses

	var err error
	var addedCourseUUID string
	if addedCourseType == "departmentCourse" {
		fmt.Println("departmentCourse")
		err = DB.Table("added_department_courses").Where("added_department_courses.department_course_id = ? AND added_department_courses.section = ?", addedCourseID, section).First(&addedDepartmentCourse).Error
		if addedDepartmentCourse.AddedDepartmentCourseID == "" {
			addedCourseUUID = uuid.NewV1().String()
			addedDepartmentCourse.AddedDepartmentCourseID = addedCourseUUID
			addedDepartmentCourse.Section = section
			addedDepartmentCourse.DepartmentCourseID = addedCourseID
			err = DB.Create(&addedDepartmentCourse).Error
		} else {
			addedCourseUUID = addedDepartmentCourse.AddedDepartmentCourseID
		}

		err = DB.Table("students").Select("students.added_department_course_ids").Where("students.student_id = ?", studentID).First(&addedDepartmentCourses).Error
		finalArray := append(addedDepartmentCourses.AddedDepartmentCourseIDs, addedDepartmentCourse.AddedDepartmentCourseID)
		fmt.Println(finalArray)
		err = DB.Model(&student).Update("added_department_course_ids", finalArray).Error

	} else if addedCourseType == "otherElectiveCourse" {
		fmt.Println("otherElectiveCourse")
		err = DB.Table("added_other_elective_courses").Where("added_other_elective_courses.other_elective_course_id = ? AND added_other_elective_courses.section = ?", addedCourseID, section).First(&addedOtherElectiveCourse).Error
		if addedOtherElectiveCourse.AddedOtherElectiveCourseID == "" {
			addedCourseUUID = uuid.NewV1().String()
			addedOtherElectiveCourse.AddedOtherElectiveCourseID = addedCourseUUID
			addedOtherElectiveCourse.Section = section
			addedOtherElectiveCourse.OtherElectiveCourseID = addedCourseID
			err = DB.Create(&addedOtherElectiveCourse).Error
		} else {
			addedCourseUUID = addedOtherElectiveCourse.AddedOtherElectiveCourseID
		}

		err = DB.Table("students").Select("students.added_other_elective_course_ids").Where("students.student_id = ?", studentID).First(&addedOtherElectiveCourses).Error
		finalArray := append(addedOtherElectiveCourses.AddedOtherElectiveCourseIDs, addedOtherElectiveCourse.AddedOtherElectiveCourseID)
		err = DB.Model(&student).Update("added_other_elective_course_ids", finalArray).Error

	} else if addedCourseType == "commonElectiveCourse" {
		fmt.Println("commonElectiveCourse")
		err = DB.Table("added_common_elective_courses").Where("added_common_elective_courses.course_id = ? AND added_common_elective_courses.section = ?", addedCourseID, section).First(&addedCommonElectiveCourse).Error
		if addedCommonElectiveCourse.AddedCommonElectiveCourseID == "" {
			addedCourseUUID = uuid.NewV1().String()
			addedCommonElectiveCourse.AddedCommonElectiveCourseID = addedCourseUUID
			addedCommonElectiveCourse.Section = section
			addedCommonElectiveCourse.CourseID = addedCourseID
			err = DB.Create(&addedCommonElectiveCourse).Error
		} else {
			addedCourseUUID = addedCommonElectiveCourse.AddedCommonElectiveCourseID
		}

		err = DB.Table("students").Select("students.added_common_elective_course_ids").Where("students.student_id = ?", studentID).First(&addedCommonElectiveCourses).Error
		finalArray := append(addedCommonElectiveCourses.AddedCommonElectiveCourseIDs, addedCommonElectiveCourse.AddedCommonElectiveCourseID)
		err = DB.Model(&student).Update("added_common_elective_course_ids", finalArray).Error

	}

	return err, addedCourseUUID

}

func DisableCourseRegistration(studentID string) error {
	var student models.Student
	err := DB.Model(&student).Where("student_id = ?", studentID).Update("course_registration_enabled", false).Error
	return err
}
