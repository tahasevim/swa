module github.com/nursena/SWA/services/user

go 1.12

require (
	github.com/golang/snappy v0.0.1 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/lib/pq v1.2.0
	github.com/satori/go.uuid v1.2.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.1.2
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
)
