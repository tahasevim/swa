package models

import (
	"github.com/lib/pq"
)

// Represents a user, we uses bson keyword to tell the mgo driver how to name
// the properties in mongodb document
type Student struct {
	StudentID                    string         `json:"studentID"`
	StudentNumber                string         `json:"studentNumber"`
	Name                         string         `json:"name"`
	Surname                      string         `json:"surname"`
	Email                        string         `json:"email"`
	Password                     string         `json:"password"`
	Year                         uint           `json:"year"`
	Term                         uint           `json:"term"`
	SubjectedCreditType          string         `json:"subjectedCreditType"`
	DepartmentID                 string         `json:"departmentID"`
	AdvisorID                    string         `json:"advisorID"`
	AddedDepartmentCourseIDs     pq.StringArray `gorm:"type:varchar(128)[]" json:"addedDepartmentCourseIDs"`
	AddedOtherElectiveCourseIDs  pq.StringArray `gorm:"type:varchar(128)[]" json:"addedOtherElectiveCourseIDs"`
	AddedCommonElectiveCourseIDs pq.StringArray `gorm:"type:varchar(128)[]" json:"addedCommonElectiveCourseIDs"`
	TakenDepartmentCourseIDs     pq.StringArray `gorm:"type:varchar(128)[]" json:"takenDepartmentCourseIDs"`
	TakenOtherElectiveCourseIDs  pq.StringArray `gorm:"type:varchar(128)[]" json:"takenOtherElectiveCourseIDs"`
	TakenCommonElectiveCourseIDs pq.StringArray `gorm:"type:varchar(128)[]" json:"takenCommonElectiveCourseIDs"`
	CourseRegistrationConfirmed  bool           `json:"courseRegistrationConfirmed"`
	CourseRegistrationEnabled    bool           `json:"courseRegistrationEnabled"`
	RemainingAkts                int            `json:"remainingAkts"`
}

type Teacher struct {
	TeacherID    string `json:"teacherID"`
	Name         string `json:"name"`
	Surname      string `json:"surname"`
	AcademicRank string `json:"academicRank"`
}

// The struct for deserialization of the json fields sent by the user as a request
// while trying to change the password.
type StudentAccountUpdateStruct struct {
	ID            string ` json:"id"`
	StudentNumber string `json:"studentNumber"`
	Email         string ` json:"email"`
	OldPassword   string ` json:"oldPassword"`
	NewPassword   string ` json:"newPassword"`
}

type FinalizeCourseRegistrationStruct struct {
	StudentID string `json:"studentID"`
}

type Students struct {
	Students []Student `json:"students"`
}

type Teachers struct {
	Teachers []Teacher `json:"teachers"`
}

type AddingCourse struct {
	AddedCourseType string `json:"addedCourseType"`
	AddedCourseID   string `json:"addedCourseID"`
	StudentID       string `json:"studentID"`
	Akts            int    `json:"akts"`
	SectionID       string `json:"sectionID"`
	Section         int    `json:"section"`
}

type RemovingCourse struct {
	RemovedCourseType string `json:"removedCourseType"`
	RemovedCourseID   string `json:"removedCourseID"`
	StudentID         string `json:"studentID"`
	Akts              int    `json:"akts"`
	SectionID         string `json:"sectionID"`
}

type ShownTakenDepartmentCourse struct {
	TakenDepartmentCourseID string         `json:"takenDepartmentCourseID"`
	DepartmentCourseID      string         `json:"departmentCourseID"`
	CourseID                string         `json:"courseID"`
	CourseCode              string         `json:"courseCode"`
	OpticCode               string         `json:"opticCode"`
	CourseName              string         `json:"courseName"`
	TheoryHour              int            `json:"theoryHour"`
	PraticsHour             int            `json:"praticsHour"`
	Credit                  int            `json:"credit"`
	Akts                    int            `json:"akts"`
	SectionIDs              pq.StringArray `json:"sectionIDs"`
	IsMust                  bool           `json:"isMust"`
	Term                    string         `json:"term"`
	FinalGrade              string         `json:"finalGrade"`
	MakeUpGrade             string         `json:"makeUpGrade"`
	IsPassed                bool           `json:"isPassed"`
}

type ShownTakenOtherElectiveCourse struct {
	TakenOtherElectiveCourseID string         `json:"takenOtherElectiveCourseID"`
	OtherElectiveCourseID      string         `json:"otherElectiveCourseID"`
	CourseID                   string         `json:"courseID"`
	CourseCode                 string         `json:"courseCode"`
	OpticCode                  string         `json:"opticCode"`
	CourseName                 string         `json:"courseName"`
	TheoryHour                 int            `json:"theoryHour"`
	PraticsHour                int            `json:"praticsHour"`
	Credit                     int            `json:"credit"`
	Akts                       int            `json:"akts"`
	SectionIDs                 pq.StringArray `json:"sectionIDs"`
	Term                       string         `json:"term"`
	FinalGrade                 string         `json:"finalGrade"`
	MakeUpGrade                string         `json:"makeUpGrade"`
	IsPassed                   bool           `json:"isPassed"`
}

type ShownTakenCommonElectiveCourse struct {
	TakenCommonElectiveCourseID string         `json:"takenCommonElectiveCourseID"`
	CourseID                    string         `json:"courseID"`
	CourseCode                  string         `json:"courseCode"`
	OpticCode                   string         `json:"opticCode"`
	CourseName                  string         `json:"courseName"`
	TheoryHour                  int            `json:"theoryHour"`
	PraticsHour                 int            `json:"praticsHour"`
	Credit                      int            `json:"credit"`
	Akts                        int            `json:"akts"`
	SectionIDs                  pq.StringArray `json:"sectionIDs"`
	Term                        string         `json:"term"`
	FinalGrade                  string         `json:"finalGrade"`
	MakeUpGrade                 string         `json:"makeUpGrade"`
	IsPassed                    bool           `json:"isPassed"`
}

type ShownAddedDepartmentCourse struct {
	AddedDepartmentCourseID string         `json:"addedDepartmentCourseID"`
	CourseID                string         `json:"courseID"`
	DepartmentCourseID      string         `json:"departmentCourseID"`
	CourseCode              string         `json:"courseCode"`
	OpticCode               string         `json:"opticCode"`
	CourseName              string         `json:"courseName"`
	TheoryHour              int            `json:"theoryHour"`
	PraticsHour             int            `json:"praticsHour"`
	Credit                  int            `json:"credit"`
	Akts                    int            `json:"akts"`
	SectionIDs              pq.StringArray `json:"sectionIDs"`
	IsMust                  bool           `json:"isMust"`
	Term                    string         `json:"term"`
	Section                 int            `json:"section"`
}

type ShownAddedOtherElectiveCourse struct {
	AddedOtherElectiveCourseID string         `json:"addedOtherElectiveCourseID"`
	CourseID                   string         `json:"courseID"`
	OtherElectiveCourseID      string         `json:"otherElectiveCourseID"`
	CourseCode                 string         `json:"courseCode"`
	OpticCode                  string         `json:"opticCode"`
	CourseName                 string         `json:"courseName"`
	TheoryHour                 int            `json:"theoryHour"`
	PraticsHour                int            `json:"praticsHour"`
	Credit                     int            `json:"credit"`
	Akts                       int            `json:"akts"`
	SectionIDs                 pq.StringArray `json:"sectionIDs"`
	Term                       string         `json:"term"`
	Section                    int            `json:"section"`
}

type ShownAddedCommonElectiveCourse struct {
	AddedCommonElectiveCourseID string         `json:"addedCommonElectiveCourseID"`
	CourseID                    string         `json:"courseID"`
	CourseCode                  string         `json:"courseCode"`
	OpticCode                   string         `json:"opticCode"`
	CourseName                  string         `json:"courseName"`
	TheoryHour                  int            `json:"theoryHour"`
	PraticsHour                 int            `json:"praticsHour"`
	Credit                      int            `json:"credit"`
	Akts                        int            `json:"akts"`
	SectionIDs                  pq.StringArray `json:"sectionIDs"`
	Section                     int            `json:"section"`
}

type AddedStudentCoursesReturn struct {
	ShownAddedDepartmentCourse     []ShownAddedDepartmentCourse     `json:"shownDepartmentCourses"`
	ShownAddedOtherElectiveCourse  []ShownAddedOtherElectiveCourse  `json:"shownOtherElectiveCourses"`
	ShownAddedCommonElectiveCourse []ShownAddedCommonElectiveCourse `json:"shownCommonElectiveCourses"`
}

type TakenStudentCoursesReturn struct {
	ShownTakenDepartmentCourse     []ShownTakenDepartmentCourse     `json:"shownTakenDepartmentCourses"`
	ShownTakenOtherElectiveCourse  []ShownTakenOtherElectiveCourse  `json:"shownTakenOtherElectiveCourses"`
	ShownTakenCommonElectiveCourse []ShownTakenCommonElectiveCourse `json:"shownTakenCommonElectiveCourses"`
}

type RemainingAktsQuery struct {
	RemainingAkts int `json:"remainingAkts"`
}

type RemainingQuotaQuery struct {
	RemainingQuota int `json:"remainingQuota"`
}

type Section struct {
	SectionID      string `json:"sectionID"`
	Quota          int    `json:"quota"`
	RemainingQuota int    `json:"remainingQuota"`
	Section        int    `json:"section"`
	TeacherID      string `json:"teacherID"`
}

type AddedDepartmentCourse struct {
	AddedDepartmentCourseID string `json:"addedDepartmentCourseID"`
	DepartmentCourseID      string `json:"departmentCourseID"`
	Section                 int    `json:"section"`
}

type AddedDepartmentCourses struct {
	AddedDepartmentCourseIDs pq.StringArray `json:"addedDepartmentCourseIDs"`
}

type AddedOtherElectiveCourse struct {
	AddedOtherElectiveCourseID string `json:"addedOtherElectiveCourseID"`
	OtherElectiveCourseID      string `json:"otherElectiveCourseID"`
	Section                    int    `json:"section"`
}

type AddedOtherElectiveCourses struct {
	AddedOtherElectiveCourseIDs pq.StringArray `json:"addedOtherElectiveCourseIDs"`
}

type AddedCommonElectiveCourse struct {
	AddedCommonElectiveCourseID string `json:"addedCommonElectiveCourseID"`
	CourseID                    string `json:"courseID"`
	Section                     int    `json:"section"`
}

type AddedCommonElectiveCourses struct {
	AddedCommonElectiveCourseIDs pq.StringArray `json:"addedCommonElectiveCourseIDs"`
}
