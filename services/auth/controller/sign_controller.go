package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/nursena/SWA/services/auth/database"
	"github.com/nursena/SWA/services/auth/models"

	"golang.org/x/crypto/bcrypt"
)

func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("login request has come")
	defer r.Body.Close()
	var student models.Student // Has not hashed password
	if err := json.NewDecoder(r.Body).Decode(&student); err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	dbUser, err := database.FindByStudentNumber(student.StudentNumber) // Has hashed password
	fmt.Println(dbUser)
	if err != nil {
		fmt.Println("There is no such user with the specified student number in the database.")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	// The salt is incorporated into the hash (as plaintext).
	// The compare function simply pulls the salt out of the hash and then uses it
	// to hash the password and perform the comparison.
	byteHashedPassword := []byte(dbUser.Password)
	bytePlainTextPassword := []byte(student.Password)
	fmt.Println(dbUser.Password)
	fmt.Println(student.Password)
	fmt.Println(byteHashedPassword)

	err = bcrypt.CompareHashAndPassword(byteHashedPassword, bytePlainTextPassword)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusUnauthorized, err.Error())
		return
	}
	// respondWithJSON(w, http.StatusOK, map[string]string{"result": "authorized"})
	respondWithJSON(w, http.StatusOK, dbUser)

}

// func Register(w http.ResponseWriter, r *http.Request) {
// 	defer r.Body.Close()
// 	var student models.Student
// 	if err := json.NewDecoder(r.Body).Decode(&student); err != nil {
// 		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
// 		return
// 	}
// 	fmt.Println(student)
// 	bytePassword := []byte(student.Password)
// 	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, 2) // n cost means 2^n rounds, should be determined according to benchmarks
// 	if err != nil {
// 		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
// 		return
// 	}
// 	student.Password = string(hashedPassword)
// 	if err := database.Insert(student); err != nil {
// 		fmt.Println("error")
// 		respondWithError(w, http.StatusInternalServerError, err.Error())
// 		return
// 	}
// 	respondWithJSON(w, http.StatusCreated, map[string]string{"result": "success"})
// }
