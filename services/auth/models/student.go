package models

import "github.com/lib/pq"

type Student struct {
	StudentID                    string         `json:"studentID"`
	StudentNumber                string         `json:"studentNumber"`
	Name                         string         `json:"name"`
	Surname                      string         `json:"surname"`
	Email                        string         `json:"email"`
	Password                     string         `json:"password"`
	Year                         uint           `json:"year"`
	Term                         uint           `json:"term"`
	SubjectedCreditType          string         `json:"subjectedCreditType"`
	DepartmentID                 string         `json:"departmentID"`
	AdvisorID                    string         `json:"advisorID"`
	AddedDepartmentCourseIDs     pq.StringArray `gorm:"type:varchar(128)[]" json:"addedDepartmentCourseIDs"`
	AddedOtherElectiveCourseIDs  pq.StringArray `gorm:"type:varchar(128)[]" json:"addedOtherElectiveCourseIDs"`
	AddedCommonElectiveCourseIDs pq.StringArray `gorm:"type:varchar(128)[]" json:"addedCommonElectiveCourseIDs"`
	TakenDepartmentCourseIDs     pq.StringArray `gorm:"type:varchar(128)[]" json:"takenDepartmentCourseIDs"`
	TakenOtherElectiveCourseIDs  pq.StringArray `gorm:"type:varchar(128)[]" json:"takenOtherElectiveCourseIDs"`
	TakenCommonElectiveCourseIDs pq.StringArray `gorm:"type:varchar(128)[]" json:"takenCommonElectiveCourseIDs"`
	CourseRegistrationConfirmed  bool           `json:"courseRegistrationConfirmed"`
	CourseRegistrationEnabled    bool           `json:"courseRegistrationEnabled"`
	RemainingAkts                int            `json:"remainingAkts"`
}
