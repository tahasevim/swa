package database

import (
	"github.com/nursena/SWA/services/auth/models"
)

const studentsTableName = "students"

// // FindAll returns the list of the students
// func FindAll() ([]*models.Student, error) {
// 	var students []*models.Student
// 	findOptions := options.Find()
// 	findOptions.SetLimit(2)

// 	cur, err := DB.Collection(studentCollection).Find(context.TODO(), bson.D{{}}, findOptions)

// 	for cur.Next(context.TODO()) {
// 		var student models.Student
// 		err := cur.Decode(&student)
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 		students = append(students, &student)
// 	}
// 	return students, err
// }

// // InsertStudent inserts a student to the database
// func Insert(student models.Student) error {
// 	student.ID = primitive.NewObjectID()
// 	_, err := DB.Collection(studentCollection).InsertOne(context.TODO(), &student)
// 	return err
// }

// // FindByID returns the student information with the specified ID
// func FindByID(id string) (models.Student, error) {
// 	var student models.Student
// 	objID, idErr := primitive.ObjectIDFromHex(id)
// 	if idErr != nil {
// 		return student, idErr
// 	}
// 	err := DB.Collection(studentCollection).FindOne(context.TODO(), bson.D{{"_id", objID}}).Decode(&student)
// 	return student, err
// }

// // FindByEmail returns the student information with the specified email
// func FindByEmail(email string) (models.Student, error) {
// 	var student models.Student
// 	err := DB.Collection(studentCollection).FindOne(context.TODO(), bson.D{{"email", email}}).Decode(&student)
// 	return student, err
// }

// FindByStudentNumber returns the student information with the specified student number
func FindByStudentNumber(studentNumber string) (models.Student, error) {
	var student models.Student
	err := DB.Table(studentsTableName).Where("student_number = ?", studentNumber).First(&student).Error

	return student, err
}

// // Delete the collection
// func DeleteAll() error {
// 	err := DB.Collection(studentCollection).Drop(context.TODO())
// 	return err
// }
