package database

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"

	_ "github.com/lib/pq"
	"github.com/nursena/SWA/services/auth/database/config"
)

var DB *gorm.DB

var dao = DAO{}

type DAO struct {
	DatabaseURI  string
	DatabaseName string
}

// Establish a connection to database
func (d *DAO) Connect() {
	connStr := os.Getenv("POSTGREDB_URI")

	db, err := gorm.Open("postgres", connStr)

	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()

	DB = db
	err = db.DB().Ping()

	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Connection to PostgreSQL established.")
}

// Parse the configuration file 'config.toml', and establish a connection to DB
func ConnectDB() {

	config := config.Config{}

	config.Read()

	dao.DatabaseURI = config.DatabaseURI
	dao.DatabaseName = config.DatabaseName
	dao.Connect()

}
