package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/nursena/SWA/services/auth/controller"
	"github.com/nursena/SWA/services/auth/database"
)

func main() {
	fmt.Println("Authentication service has just started.")
	r := mux.NewRouter()
	database.ConnectDB()
	r.HandleFunc("/api/login", controller.Login).Methods("POST")
	// r.HandleFunc("/api/register", controller.Register).Methods("POST")
	if err := http.ListenAndServe(":8080", r); err != nil {
		log.Fatal(err)
	}
}
