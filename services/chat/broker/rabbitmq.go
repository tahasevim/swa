package broker

import (
	"log"

	"github.com/streadway/amqp"
)

type MessageQueue struct {
	conn    *amqp.Connection
	channel *amqp.Channel
	queue   *amqp.Queue
}

func NewMessageQueue(address, queueName string) *MessageQueue {
	conn, err := amqp.Dial(address)
	if err != nil {
		log.Fatalln(err)
	}
	channel, err := conn.Channel()
	if err != nil {
		log.Fatalln(err)
	}
	queue, err := channel.QueueDeclare(queueName, false, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}
	return &MessageQueue{
		conn:    conn,
		channel: channel,
		queue:   &queue,
	}

}

func (m *MessageQueue) Send(msg interface{}) {
	err := m.channel.Publish("", m.queue.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: msg.([]byte)})
	if err != nil {
		log.Fatalln(err)
	}
}

func (m *MessageQueue) Receive(msgChan chan interface{}) {
	messages, err := m.channel.Consume(m.queue.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Fatalln(err)
	}
	for msg := range messages {
		msgChan <- msg.Body
	}
}
