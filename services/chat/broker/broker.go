package broker

type Broker interface {
	Send(interface{})
	Receive(chan interface{})
}
