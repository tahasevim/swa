package server

import "log"

func LogError(msg string, err error) {
	if err != nil {
		log.Fatalf("Error Ocurred: %s\nError Value: %v", msg, err)
	}
}
