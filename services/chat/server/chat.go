package server

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/nursena/SWA/services/chat/broker"
)

// ChatServer is main entity of chat service.
// Maintains connections and broker.
type ChatServer struct {
	websocket.Upgrader
	Name    string                     `json:"name"`
	Clients map[string]*websocket.Conn `json:"clients"`
	Broker  broker.Broker              `json:"broker"`
}

// Message is simple message format for communication.
type Message struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Content string `json:"content"`
}

// NewChatServer creates new ChatServer.
func NewChatServer(name string, broker broker.Broker) *ChatServer {
	return &ChatServer{
		Upgrader: websocket.Upgrader{},
		Name:     name,
		Clients:  make(map[string]*websocket.Conn),
		Broker:   broker,
	}
}

// NewMessage creates new Message.
func NewMessage(from, to, content string) *Message {
	return &Message{
		From:    from,
		To:      to,
		Content: content,
	}
}

// ConnectionHandler is a handler function to maintain communication between client and server.
// Received messages are pushed to message broker.
func (c *ChatServer) ConnectionHandler(w http.ResponseWriter, r *http.Request) {
	c.Upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	websock, err := c.Upgrade(w, r, nil)
	LogError("Upgrading connection to WebSocket Protocol is failed", err)
	defer websock.Close()

	name := r.URL.Query().Get("name")
	c.Clients[name] = websock

	log.Printf("Client %s is connected\n", name)
	log.Println(c.Clients)
	for {
		var msg Message
		err := websock.ReadJSON(&msg)
		if err != nil {
			log.Printf("Client %s is disconnected\n", name)
			break
		}
		if b, err := json.Marshal(msg); err == nil {
			log.Println(msg)
			c.Broker.Send(b)
		} else {
			LogError("Marshaling error", err)
		}
	}
	delete(c.Clients, name)
	log.Println(c.Clients)
}

// DistributeMessages function retrieves messages from broker and send them to the clients.
func (c *ChatServer) DistributeMessages() {
	msgChan := make(chan interface{}, 100)
	var msg Message

	go c.Broker.Receive(msgChan)

	for message := range msgChan {
		if err := json.Unmarshal(message.([]byte), &msg); err != nil {
			LogError("Unmarshaling error", err)
		}
		if conn, ok := c.Clients[msg.To]; ok {
			err := conn.WriteJSON(msg)
			if err != nil {
				c.Clients[msg.To].Close()
				delete(c.Clients, msg.To)
			}
		} else {
			c.Broker.Send(message.([]byte))
		}

	}
}

// Run is simple wrapper
func (c *ChatServer) Run() {
	c.DistributeMessages()
}
