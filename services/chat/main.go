package main

import (
	"log"
	"net/http"
	"os"

	"github.com/nursena/SWA/services/chat/broker"
	"github.com/nursena/SWA/services/chat/server"
)

func main() {
	brokerAddr := os.Getenv("BROKER_ADDR")
	rabbitmq := broker.NewMessageQueue(brokerAddr, "chatQueue")
	chatServer := server.NewChatServer("ChatServer", rabbitmq)
	http.HandleFunc("/chat", chatServer.ConnectionHandler)
	go chatServer.Run()
	log.Fatal(http.ListenAndServe(":8080", nil))
}
