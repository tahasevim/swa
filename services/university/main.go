package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/nursena/SWA/services/university/controller"
	"github.com/nursena/SWA/services/university/database"
)

func main() {
	r := mux.NewRouter()
	database.ConnectDB()
	r.HandleFunc("/api/university/annons", controller.GetAnnouncements).Methods("GET")
	r.HandleFunc("/api/university/academic-year", controller.GetAcademicYearInformation).Methods("GET")
	if err := http.ListenAndServe(":8080", r); err != nil {
		log.Fatal(err)
	}
}
