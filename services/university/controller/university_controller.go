package controller

import (
	"fmt"
	"net/http"

	"github.com/nursena/SWA/services/university/database"
)

func GetAcademicYearInformation(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting academic year information")
	defer r.Body.Close()
	academicYear, err := database.GetAcademicYearInformation()
	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusOK, academicYear)
}

func GetAnnouncements(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting announcements")
	defer r.Body.Close()
	announcements, err := database.GetAllAnnouncements()
	if err != nil {
		fmt.Println("error")
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusOK, announcements)

}
