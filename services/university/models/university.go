package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Represents a user, we uses bson keyword to tell the mgo driver how to name
// the properties in mongodb document
type AcademicYearInformation struct {
	ID                     primitive.ObjectID `bson:"_id" json:"id"`
	Term                   string             `bson:"term" json:"term"`
	AcademicYear           string             `bson:"academicYear" json:"academicYear"`
	CourseRegistrationDate string             `bson:"courseRegistrationDate" json:"courseRegistrationDate"`
	AddDropDate            string             `bson:"addDropDate" json:"addDropDate"`
}

type Announcement struct {
	ID           primitive.ObjectID `bson:"_id" json:"id"`
	Announcement string             `bson:"announcement" json:"announcement"`
	Date         string             `bson:"date" json:"date"`
}

type Announcements struct {
	Announcements []Announcement `json:"announcements"`
}
