package config

import (
	"errors"
	"log"
	"os"
)

// Represents database credentials
type Config struct {
	DatabaseURI  string
	DatabaseName string
}

// Read and parse the configuration file
func (c *Config) Read() {

	c.DatabaseURI = os.Getenv("MONGODB_URI")
	c.DatabaseName = os.Getenv("MONGODB_NAME")

	if c.DatabaseURI == "" {
		log.Fatal(errors.New("Empty DatabaseURI"))
	}

	if c.DatabaseURI == "" {
		log.Fatal(errors.New("Empty DatabaseName"))
	}

}
