package database

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/nursena/SWA/services/university/database/config"
	"github.com/nursena/SWA/services/university/models"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DB is the mongo database struct
var DB *mongo.Database

var dao = DAO{}

// DAO is database object
type DAO struct {
	DatabaseURI  string
	DatabaseName string
}

//InitDatabase initializes the database with predefined data
func InitDatabase(resetDB bool) {
	fmt.Println("InitDatabase")
	announcementsJSONFile, err := os.Open("database/university-info-pool/announcements.json")
	academicYearJSONFile, err := os.Open("database/university-info-pool/academic-year.json")

	defer announcementsJSONFile.Close()
	defer academicYearJSONFile.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	byteValueAnnouncements, _ := ioutil.ReadAll(announcementsJSONFile)
	byteValueAcademicYear, _ := ioutil.ReadAll(academicYearJSONFile)

	var announcements models.Announcements
	var academicYearInfo models.AcademicYearInformation

	json.Unmarshal(byteValueAnnouncements, &announcements)
	json.Unmarshal(byteValueAcademicYear, &academicYearInfo)

	if resetDB {
		// Reset Collection
		err := DeleteAcademicYearCollection()
		if err != nil {
			fmt.Println(err)
			return
		}

		err = DeleteAnnouncementsCollection()
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Println("Database is cleared.")
	}

	for i := 0; i < len(announcements.Announcements); i++ {
		announcement := announcements.Announcements[i]
		a := models.Announcement{Announcement: announcement.Announcement,
			Date: announcement.Date}
		InsertAnnouncement(a)
	}
	fmt.Println("Announcements collection is initialized.")

	academicYearObj := models.AcademicYearInformation{AcademicYear: academicYearInfo.AcademicYear,
		AddDropDate:            academicYearInfo.AddDropDate,
		CourseRegistrationDate: academicYearInfo.CourseRegistrationDate,
		Term:                   academicYearInfo.Term}

	InsertAcademicYearInformation(academicYearObj)
	fmt.Println("Academic year collection is initialized.")

}

// Connect establishes a connection to database
func (d *DAO) Connect() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(
		d.DatabaseURI))
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to University MongoDB!")
	DB = client.Database(d.DatabaseName)
}

// ConnectDB parses the configuration file 'config.toml', and establish a connection to DB
func ConnectDB() {
	initDB := true
	resetDB := true

	config := config.Config{}

	config.Read()

	dao.DatabaseURI = config.DatabaseURI
	dao.DatabaseName = config.DatabaseName
	dao.Connect()

	if initDB {
		InitDatabase(resetDB)
	}
}
