package database

import (
	"context"
	"log"

	"github.com/nursena/SWA/services/university/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const academicYearCollection = "academicYear"
const announcementsCollection = "announcements"

// FindAllAnnouncements returns the list of the announcements
func GetAllAnnouncements() ([]*models.Announcement, error) {
	var announcements []*models.Announcement
	findOptions := options.Find()
	//findOptions.SetLimit(2)

	cur, err := DB.Collection(announcementsCollection).Find(context.TODO(), bson.D{{}}, findOptions)

	for cur.Next(context.TODO()) {
		var announcement models.Announcement
		err := cur.Decode(&announcement)
		if err != nil {
			log.Fatal(err)
		}
		announcements = append(announcements, &announcement)
	}
	return announcements, err
}

// Insert an announcement into database
func InsertAnnouncement(announcement models.Announcement) error {
	announcement.ID = primitive.NewObjectID()
	_, err := DB.Collection(announcementsCollection).InsertOne(context.TODO(), &announcement)
	return err
}

// FindByID returns the announcement information with the specified ID
func FindAnnouncementByID(id string) (models.Announcement, error) {
	var announcement models.Announcement
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return announcement, err
	}
	err = DB.Collection(announcementsCollection).FindOne(context.TODO(), bson.D{{"_id", objID}}).Decode(&announcement)
	return announcement, err
}

// Delete the announcementsCollection
func DeleteAnnouncementsCollection() error {
	err := DB.Collection(announcementsCollection).Drop(context.TODO())
	return err
}

// Insert to the academicYearCollection
func InsertAcademicYearInformation(academicYear models.AcademicYearInformation) error {
	academicYear.ID = primitive.NewObjectID()
	_, err := DB.Collection(academicYearCollection).InsertOne(context.TODO(), &academicYear)
	return err
}

// Get the Academic year information
func GetAcademicYearInformation() (models.AcademicYearInformation, error) {
	var academicYear models.AcademicYearInformation
	// fix this
	err := DB.Collection(academicYearCollection).FindOne(context.TODO(), bson.D{{"academicYear", "2019-2020 Güz"}}).Decode(&academicYear)

	// c, _ := DB.Collection(academicYearCollection).Find(context.TODO(), bson.M{"$natural": -1}) // fix this
	// err := c.Decode(&academicYear)
	return academicYear, err
}

// Delete the academicYearCollection
func DeleteAcademicYearCollection() error {
	err := DB.Collection(academicYearCollection).Drop(context.TODO())
	return err
}
