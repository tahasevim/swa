import yaml
import websocket

from locust import HttpLocust, TaskSet, task, between
from utility import get_address, get_request_information

address = get_address("chatService")
request_info = get_request_information("chatService")

class ChatServiceTask(TaskSet):
    # @task
    # def chat(self):
    #     endpoint = request_info["chat"]["endpoint"]
    #     samples = request_info["chat"]["samples"]
    #     for sample in samples:
    #         ws = websocket.WebSocket()
    #         ws.connect("ws://{}/{}?name={}".format(address, endpoint, sample["from"]))
    pass
        

class ChatServiceUser(HttpLocust):
    task_set = ChatServiceTask
    wait_time = between(1, 2)
    host = "http://{}".format(address)
