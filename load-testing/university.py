import yaml

from locust import HttpLocust, TaskSet, task, between

from utility import get_address, get_request_information

address = get_address("universityService")
request_info = get_request_information("universityService")

class UniversityServiceTask(TaskSet):
    @task
    def get_announcements(self):
        endpoint = request_info["annons"]["endpoint"]
        self.client.get(endpoint)
    @task
    def get_academic_year_information(self):
        endpoint = request_info["academic-year"]["endpoint"]
        self.client.get(endpoint)


class UniversityServiceUser(HttpLocust):
    task_set = UniversityServiceTask
    wait_time = between(1, 2)
    host = "http://{}".format(address)
