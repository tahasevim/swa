import yaml

from locust import HttpLocust, TaskSet, task, between
from utility import get_address, get_request_information

address = get_address("courseService")
request_info = get_request_information("courseService")

class CourseServiceTask(TaskSet):

    @task
    def get_department_courses(self):
        endpoint = request_info["department-courses"]["endpoint"]
        samples = request_info["department-courses"]["samples"]
        for sample in samples:
            self.client.get(endpoint, headers=sample["headers"])
    @task
    def get_elective_courses(self):
        endpoint = request_info["elective-courses"]["endpoint"]
        samples = request_info["elective-courses"]["samples"]
        for sample in samples:
            self.client.get(endpoint, headers=sample["headers"])

    @task
    def get_department_and_advisor_information(self):
        endpoint = request_info["department"]["endpoint"]
        samples = request_info["department"]["samples"]
        for sample in samples:
            self.client.get(endpoint, headers=sample["headers"])
    @task
    def get_course_sections(self):
        endpoint = request_info["course-sections"]["endpoint"]
        samples = request_info["course-sections"]["samples"]
        for sample in samples:
            self.client.get(endpoint, headers=sample["headers"])


class CourseServiceUser(HttpLocust):
    task_set = CourseServiceTask
    wait_time = between(1, 2)
    host = "http://{}".format(address)
