import yaml
import resource

from locust import HttpLocust, TaskSet, task, between

from utility import get_address, get_request_information

address = get_address("userService")
request_info = get_request_information("userService")
resource.setrlimit(resource.RLIMIT_NOFILE, (10000, 10000))

class UserServiceTask(TaskSet):
    @task
    def get_added_courses(self):
        endpoint = request_info["added-courses"]["endpoint"]
        samples = request_info["added-courses"]["samples"]
        for sample in samples:
            self.client.get(endpoint, headers=sample["headers"])
    @task
    def get_taken_courses(self):
        endpoint = request_info["taken-courses"]["endpoint"]
        samples = request_info["taken-courses"]["samples"]
        for sample in samples:
            self.client.get(endpoint, headers=sample["headers"])
    @task
    def update_account_password(self):
        endpoint = request_info["password"]["endpoint"]
        samples = request_info["password"]["samples"]
        for sample in samples:
            self.client.put(endpoint, headers=sample["headers"], json=sample["body"])
    @task
    def update_account_email(self):
        endpoint = request_info["email"]["endpoint"]
        samples = request_info["email"]["samples"]
        for sample in samples:
            self.client.put(endpoint, headers=sample["headers"], json=sample["body"])

    @task
    def add_course(self):
        endpoint = request_info["add-course"]["endpoint"]
        samples = request_info["add-course"]["samples"]
        for sample in samples:
            self.client.post(endpoint, headers=sample["headers"], json=sample["body"])

    @task
    def remove_course(self):
        endpoint = request_info["remove-course"]["endpoint"]
        samples = request_info["remove-course"]["samples"]
        for sample in samples:
            self.client.put(endpoint, headers=sample["headers"], json=sample["body"])


class UserServiceUser(HttpLocust):
    task_set = UserServiceTask
    wait_time = between(1, 1)
    host = "http://{}".format(address)
