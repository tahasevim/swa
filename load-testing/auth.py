import yaml
import resource
from locust import HttpLocust, TaskSet, task, between

from utility import get_address, get_request_information

address = get_address("authService")
request_info = get_request_information("authService")
resource.setrlimit(resource.RLIMIT_NOFILE, (10000, 10000))

class AuthServiceTask(TaskSet):
    @task
    def login(self):
        endpoint = request_info["login"]["endpoint"]
        samples = request_info["login"]["samples"]
        for sample in samples:
            self.client.post(endpoint, json=sample["body"])


class AuthServiceUser(HttpLocust):
    task_set = AuthServiceTask
    wait_time = between(1, 1)
    host = "http://{}".format(address)
