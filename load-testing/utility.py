import yaml
import os
import json

DATA_PATH = "data/{}"
with open("config.yaml") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

def load_json_data(file):
    with open(file + ".json") as f:
        return json.load(f)

def get_address(service):
    return config["services"][service]["address"]

def get_request_information(service):
    return load_json_data(DATA_PATH.format(service))