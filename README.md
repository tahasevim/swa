# SWA
## Note: This repository migrated from the original repository https://github.com/nursena/SWA
## Branch name convention

### `<type>/<name>`

#### `<type>`

```
bug    - Code changes linked to a known issue.
feat   - New feature.
hotfix - Quick fixes to the codebase.
junk   - Experiments (will never be merged).
...
```

#### `<name>`

Always use dashes to seperate words, and keep it short.

#### Example branch names

```
feat/renderer-cookies
hotfix/dockerfile-base-image
bug/login-ie
```

## Commit convention

### `<verb> <object>`

#### `<verb>`

Write the summary line and description of what you have done in the imperative mode, that is as if you were commanding someone. Start the line with "Fix", "Add", "Change" instead of "Fixed", "Added", "Changed".

```
Fix
Add
Update
...
```

#### `<object>`

The object that is added, updated, fixed etc..

#### Example commits

```
Fix failing UserControllerTests
Add tests for UserController
Update docbook dependency and add product to shopping cart
```

## Setting Development Environment

### Prerequisites

#### Install Yarn

https://yarnpkg.com/lang/en/docs/install/#debian-stable

### Install repository

To install the repository run the following commands:

```bash
git clone https://github.com/nursena/SWA
cd SWA
```

This will get a copy of the project installed locally. To install all of its dependencies and start each app, follow the instructions below.

### Client

To run the client, cd into the `app` folder and run:

```bash
yarn && yarn start
```


### Executing Development Environment of Application

Boot up all services:
```bash
$ docker-compose up 
```

If you don't desire to see logs of services, execute application in detached mode:
```bash
$ docker-compose up -d  
```

### Executing Production Environment of Application

First build docker images of services from their dockerfiles (we will use a registry later on):
```bash
$ cd app && ./build-image.sh
```
```bash
$ cd services/auth && ./build-image.sh
```
After images are built, use `docker-compose.prod.yml` to boot up all services in production environment:
```bash
$ docker-compose -f docker-compose.prod.yml up  
```
### Applying Code Changes
After all services are up, you may want to change some piece of code. If the code that you want to change is able to take advantages of hot-reloading you don't need to do anything. Just change code and results will be reflected immediately.

In case of your code doesn't benefit from hot-reloading, you should restart the container of that service after you changed the code.

Restarting a container is easy since the run command of container that builds container from image will be remembered and used when restart command is applied. 

Restart a container:
```bash
$ docker restart <container-name or container id>
```
Note that we are not using any container name so that we can avoid some conflicts when we want to increase number of container instances of the services. So you can use `docker ps` to learn container id.

### Load Testing

cd into the `load-testing` folder and run:

```bash
locust -f test.py --host=http://localhost:3000
```

Go to `localhost:8089` address, enter parameters and start swarming.

### Notes

...

<!-- To solve error: "An exception occurred while running. null: InvocationTargetException: Connector configured to listen on port 8080 failed to start..."

Run: (to kill the process on some port)

```bash
sudo kill `sudo lsof -t -i:port_number`
```

For our case, it is:

```bash
sudo kill `sudo lsof -t -i:8080`
```

To resolve error "Servlet.service() for servlet [dispatcherServlet] in context with path [] ..."

Run:

```bash
./mvnw clean spring-boot:run
```

to start server. -->
