import React, { Component } from "react";
import { Card, Container, Accordion, Modal, Tab, Tabs } from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../../courseRegistration.css";
import CourseAddingPanel from "./CourseAddingPanel";
export default class AddButton extends Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }

  async componentDidMount() {}

  componentWillReceiveProps(newProps) {
    console.log("addButtonNewProps", newProps);
    console.log("addButtonOldProps", this.props);
  }

  addCourse = () => {
    console.log("addCourse");
    console.log(this.props.course);
    this.setState({ showModal: true });
  };

  hideCourseAddingPanel = () => {
    console.log("QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ");
    this.setState({ showModal: false });
  };

  componentDidUpdate() {
    console.log("componentDidUpdate");
  }

  render() {
    console.log("add button");
    return (
      <div>
        <Button
          className="addCourseButton"
          onClick={this.addCourse}
          disabled={this.props.isAddButtonDisabled}
          variant={this.props.isAddButtonDisabled ? "success" : "primary"}
        >
          +
        </Button>
        <CourseAddingPanel
          addedCourseType={this.props.addedCourseType}
          addedCourseID={this.props.addedCourseID}
          userInfo={this.props.userInfo}
          updateRemainingAkts={this.props.updateRemainingAkts}
          course={this.props.course}
          showModal={this.state.showModal}
          hideCourseAddingPanel={this.hideCourseAddingPanel}
          updateAddedCourses={this.props.updateAddedCourses}
        />
      </div>
    );
  }
}
