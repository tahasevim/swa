import React, { Component } from "react";
import { Card, Container, Accordion, Modal, Tab, Tabs } from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../../courseRegistration.css";
import CourseToBeAdded from "./CourseToBeAdded";

export default class CourseAddingPanel extends Component {
  constructor(props) {
    console.log("conistrcutor");
    super(props);

    this.state = {
      hasntEnoughRemainingCredits: "none",
      hasntChosenAnySection: "none",
      hasRegisteredSuccessfully: "none",
      courseSections: [],
      selectedCourseSection: -1
    };

    this.errors = {
      remainingAktsError:
        "Bu dersi seçebilmeniz için yeterli sayıda AKTS'niz bulunmamaktadır.",
      sectionError: "Lütfen bir şube seçiniz."
    };

    this.successes = {
      registerSuccess: "Derse başarılı bir şekilde kaydoldunuz."
    };

    this.courseSectionsAreUpdated = false;
  }

  async componentDidUpdate() {
    console.log(
      "Will update from database: ",
      this.props.showModal && !this.courseSectionsAreUpdated
    );
    if (this.props.showModal && !this.courseSectionsAreUpdated) {
      console.log(
        "fetching the course " +
          this.props.course.courseCode +
          " from database..."
      );

      var response = await fetch("/api/course-sections", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          courseID: this.props.course.courseID
        }
      });
      console.log("ID", this.props.course.courseID);

      const resp = await response.json();
      console.log("courseSections", resp);

      resp.shownCourseSections.map(
        section =>
          (section.teacherInfo =
            section.teacherAcademicRank +
            " " +
            section.teacherName +
            " " +
            section.teacherSurname)
      );

      this.setState({
        courseSections: resp.shownCourseSections
      });

      this.courseSectionsAreUpdated = true;
    }
  }

  changeSelectedSection = selectedSection => {
    this.setState({ selectedCourseSection: selectedSection });
  };

  async requestCourseRegistration() {
    var sectionID = this.state.courseSections.filter(
      obj => obj.section == this.state.selectedCourseSection
    )[0].sectionID;
    console.log(sectionID);
    console.log(this.props.addedCourseType);
    console.log(this.props.addedCourseID);
    return fetch("/api/user/add-course", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        addedCourseType: this.props.addedCourseType,
        addedCourseID: this.props.addedCourseID,
        studentID: this.props.userInfo.studentID,
        akts: this.props.course.akts,
        section: this.state.selectedCourseSection,
        sectionID: sectionID
      })
    }).then(response => {
      return response;
    });
  }

  registerToCourse = async event => {
    event.preventDefault();
    console.log(
      "trying to register the course: " +
        this.props.course.courseCode +
        " with section: " +
        this.state.selectedCourseSection
    );
    if (this.state.selectedCourseSection == -1) {
      this.setState({ hasntChosenAnySection: true });
    } else {
      console.log("call request sending method");
      var response = await this.requestCourseRegistration();
      const responseJSON = await response.json();
      console.log(responseJSON, "responseJSON");
      if (response.status == 201) {
        console.log("Registered to the course.");
        this.setState({ hasRegisteredSuccessfully: true });
        // var hidePanel = this.hidePanel;
        this.props.updateRemainingAkts(this.props.course.akts);
        // Update the UI
        var finalCourseObj = this.props.course;
        finalCourseObj.section = this.state.selectedCourseSection;
        finalCourseObj.selectedSectionID = this.state.courseSections.filter(
          obj => obj.section == this.state.selectedCourseSection
        )[0].sectionID;
        if (this.props.addedCourseType == "departmentCourse") {
          finalCourseObj.addedDepartmentCourseID = responseJSON.addedCourseUUID;
        } else if (this.props.addedCourseType == "otherElectiveCourse") {
          finalCourseObj.addedOtherElectiveCourseID =
            responseJSON.addedCourseUUID;
        } else if (this.props.addedCourseType == "commonElectiveCourse") {
          finalCourseObj.addedCommonElectiveCourseID =
            responseJSON.addedCourseUUID;
        }
        this.props.updateAddedCourses(
          finalCourseObj,
          this.props.addedCourseType,
          this.props.userInfo
        );
        // setTimeout(function() {
        //   hidePanel();
        // }, 20000);
      } else if (response.status == 406) {
        console.log("There is no remaining akts.");
        this.setState({ hasntEnoughRemainingCredits: true });
      } else if (response.status == 403) {
        console.log("The remaining quota of the selected section is zero.");
      } else if (response.status == 410) {
        console.log("Some error is occurred, failed.");
      }
    }

    // var isRegistrationSuccessful = true; // request made
    // if (isRegistrationSuccessful) {
    //   // change the add button of the course with the disabled button appearance

    //   var takenCourse = this.props.course;
    //   takenCourse.section = this.state.selectedCourseSection;

    //   this.props.updateTakenClasses(takenCourse);
    // } else {
    //   alert("Almaya çalıştığınız dersin kotası doludur");
    // }
  };

  hidePanel = () => {
    this.courseSectionsAreUpdated = false;
    this.setState({
      hasntEnoughRemainingCredits: "none",
      hasntChosenAnySection: "none",
      hasRegisteredSuccessfully: "none",
      selectedCourseSection: -1
    });
    this.props.hideCourseAddingPanel();
  };

  render() {
    console.log(this.state.selectedCourseSection);
    return (
      <div>
        <Modal
          show={this.props.showModal}
          onHide={this.hidePanel}
          size="lg"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>
              {" "}
              {this.props.course.courseName} ({this.props.course.courseCode})
              DERS KAYDI
            </Modal.Title>{" "}
          </Modal.Header>
          <Modal.Body>
            <div>
              <Container>
                <Accordion defaultActiveKey="1" className="course-modal-panel">
                  <Card>
                    <CourseToBeAdded
                      courseSection={this.state.courseSections}
                      changeSelectedSection={this.changeSelectedSection}
                    />
                  </Card>
                </Accordion>
              </Container>
            </div>
          </Modal.Body>
          <div
            style={{
              display: this.state.hasntEnoughRemainingCredits,
              color: "red",
              margin: "auto"
            }}
          >
            <b>{this.errors.remainingAktsError}</b>
          </div>
          <div
            style={{
              display: this.state.hasntChosenAnySection,
              color: "red",
              margin: "auto"
            }}
          >
            <b>{this.errors.sectionError}</b>
          </div>
          <div
            style={{
              display: this.state.hasRegisteredSuccessfully,
              color: "green",
              margin: "auto"
            }}
          >
            <b>{this.successes.registerSuccess}</b>
          </div>

          <Modal.Footer>
            {/* change the akts value */}
            <Button
              variant="primary"
              onClick={this.registerToCourse}
              disabled={
                true ? this.state.hasRegisteredSuccessfully == true : false
              }
            >
              Kaydol
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
