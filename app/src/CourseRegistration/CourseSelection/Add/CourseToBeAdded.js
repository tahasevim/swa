import React, { Component } from "react";
import { Card, Container, Accordion, Modal, Tab, Tabs } from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../../courseRegistration.css";

const columns = [
  {
    dataField: "section",
    text: "Şube",
    sort: true
  },
  {
    dataField: "quota",
    text: "Kota",
    sort: true
  },
  {
    dataField: "teacherInfo",
    text: "Ders Sorumlusu",
    sort: true
  }
];

export default class CourseToBeAdded extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const selectRow = {
      mode: "radio",
      clickToSelect: true,
      style: { background: "rgb(145,191,238)" },
      onSelect: (row, isSelect, rowIndex, e) => {
        this.props.changeSelectedSection(row.section);
      }
    };

    console.log(this);
    return (
      <div>
        <BootstrapTable
          bootstrap4
          keyField="section"
          data={this.props.courseSection}
          selectRow={selectRow}
          columns={columns}
        />
      </div>
    );
  }
}
