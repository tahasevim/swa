import React, { Component } from "react";
import { Card, Container, Accordion, Modal, Tab, Tabs } from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../../courseRegistration.css";
import CourseRemovingPanel from "../Remove/CourseRemovingPanel";

export default class RemoveButton extends Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }

  async componentDidMount() {}

  removeCourse = () => {
    this.setState({ showModal: true });
  };

  hideCourseRemovingPanel = () => {
    this.setState({ showModal: false });
  };

  render() {
    console.log("removeButton");
    return (
      <div>
        <Button
          className="removeCourseButton"
          variant="danger"
          onClick={this.removeCourse}
        >
          -
        </Button>
        <CourseRemovingPanel
          userInfo={this.props.userInfo}
          updateRemainingAkts={this.props.updateRemainingAkts}
          showModal={this.state.showModal}
          hideCourseRemovingPanel={this.hideCourseRemovingPanel}
          courseToBeRemoved={this.props.courseToBeRemoved}
          removeCourseFromAddedClassesTable={
            this.props.removeCourseFromAddedClassesTable
          }
          addedCourseType={this.props.addedCourseType}
        />
      </div>
    );
  }
}
