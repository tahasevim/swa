import React, { Component } from "react";
import { Card, Container, Accordion, Modal, Tab, Tabs } from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../../courseRegistration.css";

export default class CourseRemovingPanel extends Component {
  constructor(props) {
    super(props);
  }

  async removeCourseFromDatabase() {
    var courseToBeRemoved = this.props.courseToBeRemoved;
    var courseID;
    if (this.props.addedCourseType == "departmentCourse") {
      courseID = courseToBeRemoved.addedDepartmentCourseID;
    } else if (this.props.addedCourseType == "otherElectiveCourse") {
      courseID = courseToBeRemoved.addedOtherElectiveCourseID;
    } else if (this.props.addedCourseType == "commonElectiveCourse") {
      courseID = courseToBeRemoved.addedCommonElectiveCourseID;
    }
    console.log("Removed course id is : ", courseID);
    return fetch("/api/user/remove-course", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        removedCourseType: this.props.addedCourseType,
        removedCourseID: courseID,
        studentID: this.props.userInfo.studentID,
        akts: this.props.courseToBeRemoved.akts,
        sectionID: courseToBeRemoved.selectedSectionID
      })
    }).then(response => {
      return response;
    });
  }

  async courseRemovingConfirmationEvent(courseAkts) {
    var response = await this.removeCourseFromDatabase();
    if (response.status == 201) {
      console.log("successfully deleted");
    }

    this.props.updateRemainingAkts(courseAkts);
    this.props.removeCourseFromAddedClassesTable(
      this.props.courseToBeRemoved,
      this.props.addedCourseType
    );
    this.props.hideCourseRemovingPanel();
  }

  render() {
    return (
      <div>
        <Modal
          show={this.props.showModal}
          onHide={this.props.hideCourseAddingPanel}
          size="lg"
          centered
        >
          {/* Pass the name of the course to be deleted */}
          <Modal.Header>
            <Modal.Title>
              {this.props.courseToBeRemoved.courseName} dersini silmek
              istediğinizden emin misiniz ?
            </Modal.Title>{" "}
            {/* Change */}
          </Modal.Header>
          <Modal.Footer>
            {/* pass the minus akts of the course and remove the class from the table*/}
            <Button
              variant="primary"
              onClick={() =>
                this.courseRemovingConfirmationEvent(
                  -this.props.courseToBeRemoved.akts
                )
              }
            >
              Evet
            </Button>
            <Button
              variant="danger"
              onClick={this.props.hideCourseRemovingPanel}
            >
              Hayır
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
