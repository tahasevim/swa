import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Table
} from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../courseRegistration.css";

const columns = [
  {
    dataField: "rank",
    text: "#",
    sort: true
  },
  {
    dataField: "processes",
    text: "İşlemler",
    sort: true
  },
  {
    dataField: "courseCode",
    text: "Ders Kodu",
    sort: true
  },
  {
    dataField: "opticCode",
    text: "Optik Kodu",
    sort: true
  },
  {
    dataField: "courseName",
    text: "Ders Adı",
    sort: true
  },
  {
    dataField: "theoryHour",
    text: "T",
    sort: true
  },
  {
    dataField: "praticsHour",
    text: "P",
    sort: true
  },
  {
    dataField: "credit",
    text: "K",
    sort: true
  },
  {
    dataField: "akts",
    text: "AKTS",
    sort: true
  },
  {
    dataField: "finalGrade",
    text: "G.N",
    sort: true
  },
  {
    dataField: "makeUpGrade",
    text: "B.N",
    sort: true
  },
  {
    dataField: "mustOrElective",
    text: "Z/S",
    sort: true
  }
];

export default class ElectiveClassesTable extends Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, rows: [] };
  }

  addCourse = () => {
    this.setState({ showModal: true });
  };

  hideCourseAddingPanel = () => {
    this.setState({ showModal: false });
  };

  finalTableRow = electiveClasses => {
    var rank = 1;
    for (var course of electiveClasses) {
      course.rank = rank;
      rank += 1;
    }
    return electiveClasses;
  };

  finalTableColumn = type => {
    console.log(columns);
    console.log(type);
    if (type != "takenCourses") {
      if (columns != null) {
        console.log("didReturn");
        var modifiedColumns = columns.filter(
          obj => obj.dataField != "finalGrade"
        );
        var modifiedColumnsLast = modifiedColumns.filter(
          obj => obj.dataField != "makeUpGrade"
        );
        console.log(modifiedColumnsLast);
        return modifiedColumnsLast;
      }
    }
    return columns;
  };

  render() {
    return (
      <div>
        <BootstrapTable
          bootstrap4
          keyField="id"
          data={this.finalTableRow(this.props.electiveClasses)}
          columns={this.finalTableColumn(this.props.type)}
        />
      </div>
    );
  }
}
