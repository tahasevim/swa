import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Tabs,
  Tab
} from "react-bootstrap";

import { Button } from "react-bootstrap";
import "../courseRegistration.css";
import ElectiveClassesTable from "./ElectiveClassesTable";
import AddButton from "../CourseSelection/Add/AddButton";

export default class ElectiveClassesAccordion extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  // async componentWillMount() {
  //   console.log("Elective Course Fetching");
  //   var response = await fetch("/api/elective-courses", {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //       departmentID: this.props.userInfo.departmentID
  //     }
  //   });

  //   const resp = await response.json();
  //   console.log("electiveCourses", resp);

  //   var otherElectiveClassesWithButtons = [];
  //   for (var otherElectiveCourse of resp.shownOtherElectiveCourses) {
  //     otherElectiveCourse.mustOrElective = "Seçmeli";
  //     otherElectiveClassesWithButtons.push(
  //       this.getCourseRowWithButton(otherElectiveCourse)
  //     );
  //   }

  //   var commonElectiveClassesWithButtons = [];
  //   for (var commonElectiveCourse of resp.shownCommonElectiveCourses) {
  //     commonElectiveCourse.mustOrElective = "Seçmeli";
  //     commonElectiveClassesWithButtons.push(
  //       this.getCourseRowWithButton(commonElectiveCourse)
  //     );
  //   }

  //   this.setState({
  //     otherElectiveClasses: otherElectiveClassesWithButtons,
  //     commonElectiveClasses: commonElectiveClassesWithButtons
  //   });
  // }

  // getCourseRowWithButton = courseObj => {
  //   courseObj.processes = (
  //     <AddButton
  //       updateRemainingAkts={this.props.updateRemainingAkts}
  //       course={courseObj}
  //       getCourseFromButtonCourseCode={this.getCourseFromButtonCourseCode}
  //     />
  //   );
  //   return courseObj;
  // };

  render() {
    return (
      <div>
        <Accordion defaultActiveKey="0" className="course-accordion">
          <Card>
            <Card.Header>
              <Accordion.Toggle
                as={Button}
                className="course-accordion-item-header"
                variant="link"
                eventKey="2"
              >
                Öğretim Programı Dışındaki Dersler
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="2">
              <Tabs
                defaultActiveKey="takenCourses"
                id="uncontrolled-tab-example"
                className="terms-tabs-container"
              >
                <Tab eventKey="takenCourses" title="Alınmış Dersler">
                  <ElectiveClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    electiveClasses={this.props.takenElectiveCourses}
                    type="takenCourses"
                  />
                </Tab>
                <Tab eventKey="SECCourses" title="SEÇ Kodlu Dersler">
                  <ElectiveClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    electiveClasses={this.props.commonElectiveClasses}
                    type="SECCourses"
                  />
                </Tab>
                <Tab
                  eventKey="otherProgramCourses"
                  title="Diğer Program Dersleri"
                >
                  <ElectiveClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    electiveClasses={this.props.otherElectiveClasses}
                    type="otherProgramCourses"
                  />
                </Tab>
              </Tabs>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
    );
  }
}
