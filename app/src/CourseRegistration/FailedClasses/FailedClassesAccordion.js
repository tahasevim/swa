import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion
} from "react-bootstrap";

import { Button } from "react-bootstrap";
import "../courseRegistration.css";
import FailedClassesTable from "./FailedClassesTable";

export default class FailedClassesAccordion extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Accordion className="course-accordion">
          <Card>
            <Card.Header>
              <Accordion.Toggle
                as={Button}
                className="course-accordion-item-header"
                variant="link"
                eventKey="0"
              >
                Başarısız Olunan Bölüm Dersleri
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <FailedClassesTable
                updateRemainingAkts={this.props.updateRemainingAkts}
                updateTakenClasses={this.props.updateTakenClasses}
                takenClasses={this.props.takenClasses}
                takenFailedDepartmentCourses={
                  this.props.takenFailedDepartmentCourses
                }
              />
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
    );
  }
}
