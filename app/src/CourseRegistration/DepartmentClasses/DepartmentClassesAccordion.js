import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Tab,
  Tabs
} from "react-bootstrap";
import AddButton from "../CourseSelection/Add/AddButton";

import { Button } from "react-bootstrap";
import DepartmentClassesTable from "./DepartmentClassesTable";

import "../courseRegistration.css";

export default class DepartmentClassesAccordion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentClasses: []
    };
  }

  // async componentDidMount() {
  //   console.log("Deparment Course Fetching");
  //   console.log(this.props);
  //   var response = await fetch("/api/department-courses", {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //       departmentID: this.props.userInfo.departmentID
  //     }
  //   });

  //   const resp = await response.json();

  //   var classesWithButtons = [];
  //   var rankNum = 1;
  //   for (var course of resp.shownDepartmentCourses) {
  //     if (course.isMust) {
  //       course.mustOrElective = "Zorunlu";
  //     } else {
  //       course.mustOrElective = "Seçmeli";
  //     }
  //     course.rank = rankNum;
  //     classesWithButtons.push(this.getCourseRowWithButton(course));
  //     rankNum += 1;
  //   }

  //   this.setState({ departmentClasses: classesWithButtons });
  // }

  // getCourseRowWithButton = courseObj => {
  //   courseObj.processes = (
  //     <AddButton
  //       updateRemainingAkts={this.props.updateRemainingAkts}
  //       course={courseObj}
  //     />
  //   );
  //   return courseObj;
  // };

  render() {
    console.log("MustClassesAccordion", this.props.departmentCourses);
    return (
      <div>
        <Accordion defaultActiveKey="0" className="must-course-accordion">
          <Card>
            <Card.Header>
              <Accordion.Toggle
                as={Button}
                className="must-course-accordion-item-header"
                variant="link"
                eventKey="1"
              >
                Öğretim Programı Dersleri
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
              <Tabs
                defaultActiveKey="firstTerm"
                id="uncontrolled-tab-example"
                className="terms-tabs-container"
              >
                <Tab eventKey="firstTerm" title="1.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "1.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="secondTerm" title="2.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "2.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="thirdTerm" title="3.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "3.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="forthTerm" title="4.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "4.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="fifthTerm" title="5.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "5.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="sixthTerm" title="6.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "6.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="seventhTerm" title="7.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "7.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="eighthTerm" title="8.YY">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust && course.term == "8.YY"
                    )}
                  />
                </Tab>
                <Tab eventKey="technicalElectives" title="Diğer">
                  <DepartmentClassesTable
                    updateRemainingAkts={this.props.updateRemainingAkts}
                    departmentClasses={this.props.departmentCourses.filter(
                      course => course.isMust == false
                    )}
                  />
                </Tab>
              </Tabs>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
    );
  }
}
