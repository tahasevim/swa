import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Table
} from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../courseRegistration.css";

const columns = [
  {
    dataField: "rank",
    text: "#",
    sort: true
  },
  {
    dataField: "processes",
    text: "İşlemler",
    sort: true
  },
  {
    dataField: "courseCode",
    text: "Ders Kodu",
    sort: true
  },
  {
    dataField: "opticCode",
    text: "Optik Kodu",
    sort: true
  },
  {
    dataField: "courseName",
    text: "Ders Adı",
    sort: true
  },
  {
    dataField: "theoryHour",
    text: "T",
    sort: true
  },
  {
    dataField: "praticsHour",
    text: "P",
    sort: true
  },
  {
    dataField: "credit",
    text: "K",
    sort: true
  },
  {
    dataField: "akts",
    text: "AKTS",
    sort: true
  },
  {
    dataField: "mustOrElective",
    text: "Z/S",
    sort: true
  },
  {
    dataField: "finalGrade",
    text: "G.N",
    sort: true
  },
  {
    dataField: "makeUpGrade",
    text: "B.N",
    sort: true
  }
];

export default class MustClassesTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
  }

  addCourse = () => {
    this.setState({ showModal: true });
  };

  hideCourseAddingPanel = () => {
    this.setState({ showModal: false });
  };

  finalTableRow = departmentClasses => {
    var rank = 1;
    for (var course of departmentClasses) {
      course.rank = rank;
      rank += 1;
    }
    return departmentClasses;
  };

  render() {
    return (
      <div>
        <BootstrapTable
          bootstrap4
          keyField="id"
          data={this.finalTableRow(this.props.departmentClasses)}
          columns={columns}
        />
      </div>
    );
  }
}
