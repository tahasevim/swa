import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion
} from "react-bootstrap";

import NavBar from "../Bars/NavBar";
import SideBar from "../Bars/SideBar";
import FailedClassesAccordion from "./FailedClasses/FailedClassesAccordion";
import DepartmentClassesAccordion from "./DepartmentClasses/DepartmentClassesAccordion";
import ElectiveClassesAccordion from "./ElectiveClasses/ElectiveClassesAccordion";
import TakenClassesAccordion from "./TakenClasses/TakenClassesAccordion";
import { Button } from "react-bootstrap";
import AddButton from "./CourseSelection/Add/AddButton";
import RemoveButton from "./CourseSelection/Remove/RemoveButton";

import "./courseRegistration.css";

export default class CourseRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCourseRegistrationEnabled: this.props.location.userInfo
        .courseRegistrationEnabled,
      remainingAkts: this.props.location.userInfo.remainingAkts,
      takenClasses: [],
      addedCourses: {
        addedDepartmentCourses: [],
        addedOtherElectiveCourses: [],
        addedCommonElectiveCourses: []
      },
      takenCourses: {
        takenDepartmentCourses: [],
        takenOtherElectiveCourses: [],
        takenCommonElectiveCourses: []
      },
      departmentCourses: [],
      electiveCourses: { otherElectiveCourses: [], commonElectiveCourses: [] }
    };
  }

  async componentWillMount() {
    console.log("Added Courses Fetching");
    var response = await fetch("/api/user/added-courses", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        studentID: this.props.location.userInfo.studentID
      }
    });

    const resp = await response.json();
    console.log("addedCourses", resp);

    console.log("Taken Courses Fetching");
    var response2 = await fetch("/api/user/taken-courses", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        studentID: this.props.location.userInfo.studentID
      }
    });

    const resp2 = await response2.json();
    console.log("takenCourses", resp2);

    var response3 = await fetch("/api/department-courses", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        departmentID: this.props.location.userInfo.departmentID
      }
    });

    const resp3 = await response3.json();
    console.log("departmentCourses", resp3);

    var response4 = await fetch("/api/elective-courses", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        departmentID: this.props.location.userInfo.departmentID
      }
    });

    const resp4 = await response4.json();
    console.log("electiveCourses", resp4);

    this.preprocessCourseObjects(
      resp4.shownOtherElectiveCourses,
      false,
      "otherElectiveCourse",
      resp.shownOtherElectiveCourses
    );
    this.preprocessCourseObjects(
      resp4.shownCommonElectiveCourses,
      false,
      "commonElectiveCourse",
      resp.shownCommonElectiveCourses
    );

    this.preprocessCourseObjects(
      resp3.shownDepartmentCourses,
      false,
      "departmentCourse",
      resp.shownDepartmentCourses
    );

    this.preprocessCourseObjects(
      resp2.shownTakenDepartmentCourses,
      false,
      "departmentCourse",
      resp.shownDepartmentCourses
    );
    this.preprocessCourseObjects(
      resp2.shownTakenOtherElectiveCourses,
      false,
      "otherElectiveCourse",
      resp.shownOtherElectiveCourses
    );
    this.preprocessCourseObjects(
      resp2.shownTakenCommonElectiveCourses,
      false,
      "commonElectiveCourse",
      resp.shownCommonElectiveCourses
    );

    this.preprocessCourseObjects(
      resp.shownDepartmentCourses,
      true,
      "departmentCourse",
      null
    ); // shownAddedDepartmentCourses
    this.preprocessCourseObjects(
      resp.shownOtherElectiveCourses,
      true,
      "otherElectiveCourse",
      null
    ); // shownAddedOtherElectiveCourses
    this.preprocessCourseObjects(
      resp.shownCommonElectiveCourses,
      true,
      "commonElectiveCourse",
      null
    ); // shownAddedCommonElectiveCourses

    resp3.shownDepartmentCourses = this.eliminateTakenCoursesFromDepartmentCourses(
      resp3.shownDepartmentCourses,
      resp2.shownTakenDepartmentCourses
    );

    resp4.shownOtherElectiveCourses = this.eliminateTakenCoursesFromElectiveCourses(
      resp4.shownOtherElectiveCourses,
      resp2.shownTakenOtherElectiveCourses
    );

    resp4.shownCommonElectiveCourses = this.eliminateTakenCoursesFromElectiveCourses(
      resp4.shownCommonElectiveCourses,
      resp2.shownTakenCommonElectiveCourses
    );

    this.setState({
      addedCourses: {
        addedDepartmentCourses: resp.shownDepartmentCourses,
        addedOtherElectiveCourses: resp.shownOtherElectiveCourses,
        addedCommonElectiveCourses: resp.shownCommonElectiveCourses
      },
      takenCourses: {
        takenDepartmentCourses: resp2.shownTakenDepartmentCourses,
        takenOtherElectiveCourses: resp2.shownTakenOtherElectiveCourses,
        takenCommonElectiveCourses: resp2.shownTakenCommonElectiveCourses
      },
      departmentCourses: resp3.shownDepartmentCourses,
      electiveCourses: {
        otherElectiveCourses: resp4.shownOtherElectiveCourses,
        commonElectiveCourses: resp4.shownCommonElectiveCourses
      }
    });
  }

  eliminateTakenCoursesFromElectiveCourses(electiveCourses, takenCourses) {
    var finalElectiveCourses = [];
    for (var eCourse of electiveCourses) {
      var putToFinalArray = true;
      for (var tCourse of takenCourses) {
        if (eCourse.courseCode == tCourse.courseCode) {
          putToFinalArray = false;
          break;
        }
      }
      if (putToFinalArray) {
        finalElectiveCourses.push(eCourse);
      }
    }

    return finalElectiveCourses;
  }

  eliminateTakenCoursesFromDepartmentCourses(departmentCourses, takenCourses) {
    var finalDepartmentCourses = [];
    for (var dCourse of departmentCourses) {
      var putToFinalArray = true;
      var tCourseToBeAdded = null;
      for (var tCourse of takenCourses) {
        if (dCourse.courseCode == tCourse.courseCode) {
          if (!tCourse.isPassed) {
            putToFinalArray = false;
            break;
          } else {
            tCourseToBeAdded = tCourse;
          }
        }
      }
      if (putToFinalArray) {
        if (tCourseToBeAdded != null) finalDepartmentCourses.push(tCourse);
        else finalDepartmentCourses.push(dCourse);
      }
    }

    return finalDepartmentCourses;
  }

  preprocessCourseObjects(
    courseArray,
    isAddedCourseArray,
    courseType,
    addedCourses
  ) {
    for (var course of courseArray) {
      var courseID;
      if (courseType == "departmentCourse") {
        courseID = course.departmentCourseID;
      } else if (courseType == "otherElectiveCourse") {
        courseID = course.otherElectiveCourseID;
      } else if (courseType == "commonElectiveCourse") {
        courseID = course.courseID;
      }
      if (course.isMust) {
        course.mustOrElective = "Zorunlu";
      } else {
        course.mustOrElective = "Seçmeli";
      }
      if (isAddedCourseArray) {
        this.getCourseWithRemoveButton(
          course,
          courseType,
          this.props.location.userInfo
        ); // add addedDepartmentCourseID vs. info
      } else {
        this.getCourseRowWithButton(course, courseType, courseID, addedCourses);
      }
    }
  }

  getCourseRowWithButton = (courseObj, courseType, courseID, addedCourses) => {
    var disableAddButton = false;
    for (var addedCourse of addedCourses) {
      if (addedCourse.courseCode == courseObj.courseCode) {
        disableAddButton = true;
        break;
      }
    }
    if (courseObj.courseCode)
      courseObj.processes = (
        <AddButton
          userInfo={this.props.location.userInfo}
          updateRemainingAkts={this.updateRemainingAkts}
          course={courseObj}
          updateAddedCourses={this.updateAddedCourses}
          isAddButtonDisabled={disableAddButton}
          addedCourseType={courseType}
          addedCourseID={courseID}
        />
      );
    return courseObj;
  };

  getCourseWithDisabledButton = (courseObj, courseType, courseID, userInfo) => {
    courseObj.processes = (
      <AddButton
        key={courseObj.courseCode}
        userInfo={userInfo}
        updateRemainingAkts={this.updateRemainingAkts}
        course={courseObj}
        updateAddedCourses={this.updateAddedCourses}
        isAddButtonDisabled={true}
        addedCourseType={courseType}
        addedCourseID={courseID}
      />
    );
  };

  getCourseWithRemoveButton = (courseObj, courseType, userInfo) => {
    courseObj.removeProcesses = (
      <div>
        <RemoveButton
          userInfo={userInfo}
          addedCourseType={courseType}
          updateRemainingAkts={this.updateRemainingAkts}
          courseToBeRemoved={courseObj}
          removeCourseFromAddedClassesTable={
            this.removeCourseFromAddedClassesTable
          }
        />
      </div>
    );
  };

  async requestFinalizeCourseRegistration() {
    return fetch("/api/user/complete-course-registration", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        studentID: this.props.location.userInfo.studentID
      })
    }).then(response => {
      return response;
    });
  }

  async finalizeCourseRegistration() {
    var response = await this.requestFinalizeCourseRegistration();
    if (response.status == 200) {
      this.setState({ isCourseRegistrationEnabled: false });
      console.log("course registration status successfully updated");
    }
  }

  updateRemainingAkts = courseAkts => {
    console.log("updateRemainingAkts", courseAkts);
    if (this.state.remainingAkts >= courseAkts) {
      this.props.location.userInfo.remainingAkts -= courseAkts;
      this.setState({ remainingAkts: this.state.remainingAkts - courseAkts });
      return true;
    } else {
      return false;
    }
  };

  disableCoursesButtons = (
    courseObj,
    courseArray,
    courseType,
    courseID,
    userInfo
  ) => {
    var newCourses = courseArray.map(a => ({
      ...a
    }));
    var ind = 0;
    for (var course of newCourses) {
      if (course.courseCode == courseObj.courseCode) {
        this.getCourseWithDisabledButton(
          course,
          courseType,
          courseID,
          userInfo
        );
        newCourses[ind] = course;
        break;
      }
      ind++;
    }
    return newCourses;
  };

  updateAddedCourses = (courseObj, courseType, userInfo) => {
    console.log("updateAddedCourses");
    console.log(courseObj);

    var addedCoursesObj = this.state.addedCourses;
    var newAddedCoursesArray;

    this.getCourseWithRemoveButton(courseObj, courseType, userInfo); // add addedDepartmentCourseID vs. info

    var newTakenCourses = this.state.takenCourses;

    if (courseType == "departmentCourse") {
      newAddedCoursesArray = addedCoursesObj.addedDepartmentCourses;
      newAddedCoursesArray.push(courseObj);
      addedCoursesObj.addedDepartmentCourses = newAddedCoursesArray;

      var newDepartmentCourses = this.disableCoursesButtons(
        courseObj,
        this.state.departmentCourses,
        courseType,
        courseObj.departmentCourseID,
        userInfo
      );

      var newTakenDepartmentCourses = this.disableCoursesButtons(
        courseObj,
        this.state.takenCourses.takenDepartmentCourses,
        courseType,
        courseObj.departmentCourseID,
        userInfo
      );

      newTakenCourses.takenDepartmentCourses = newTakenDepartmentCourses;

      this.setState({
        departmentCourses: newDepartmentCourses,
        takenCourses: newTakenCourses
      });
    } else if (courseType == "otherElectiveCourse") {
      newAddedCoursesArray = addedCoursesObj.addedOtherElectiveCourses;
      newAddedCoursesArray.push(courseObj);
      addedCoursesObj.addedOtherElectiveCourses = newAddedCoursesArray;

      var newElectiveCourses = this.state.electiveCourses;

      var newOtherElectiveCourses = this.disableCoursesButtons(
        courseObj,
        newElectiveCourses.otherElectiveCourses,
        courseType,
        courseObj.otherElectiveCourseID,
        userInfo
      );

      newElectiveCourses.otherElectiveCourses = newOtherElectiveCourses;

      var newTakenOtherElectiveCourses = this.disableCoursesButtons(
        courseObj,
        newTakenCourses.takenOtherElectiveCourses,
        courseType,
        courseObj.otherElectiveCourseID,
        userInfo
      );

      newTakenCourses.takenOtherElectiveCourses = newTakenOtherElectiveCourses;

      this.setState({
        takenCourses: newTakenCourses,
        electiveCourses: newElectiveCourses
      });
    } else if (courseType == "commonElectiveCourse") {
      newAddedCoursesArray = addedCoursesObj.addedCommonElectiveCourses;
      newAddedCoursesArray.push(courseObj);
      addedCoursesObj.addedCommonElectiveCourses = newAddedCoursesArray;

      var newElectiveCourses = this.state.electiveCourses;

      var newCommonElectiveCourses = this.disableCoursesButtons(
        courseObj,
        newElectiveCourses.commonElectiveCourses,
        courseType,
        courseObj.courseID,
        userInfo
      );

      newElectiveCourses.commonElectiveCourses = newCommonElectiveCourses;

      var newTakenCommonElectiveCourses = this.disableCoursesButtons(
        courseObj,
        newTakenCourses.takenCommonElectiveCourses,
        courseType,
        courseObj.courseID,
        userInfo
      );

      newTakenCourses.takenCommonElectiveCourses = newTakenCommonElectiveCourses;

      this.setState({
        takenCourses: newTakenCourses,
        electiveCourses: newElectiveCourses
      });
    }

    this.setState({ addedCourses: addedCoursesObj });
  };

  enableCoursesButtons = (courseObj, courseArray, courseType, courseID) => {
    var newCourses = courseArray.map(a => ({
      ...a
    }));
    var ind = 0;
    for (var course of newCourses) {
      if (course.courseCode == courseObj.courseCode) {
        course.processes = (
          <AddButton
            userInfo={this.props.location.userInfo}
            updateRemainingAkts={this.updateRemainingAkts}
            course={courseObj}
            updateAddedCourses={this.updateAddedCourses}
            isAddButtonDisabled={false}
            addedCourseType={courseType}
            addedCourseID={courseID}
          />
        );
        newCourses[ind] = course;
        break;
      }
      ind++;
    }
    return newCourses;
  };

  removeCourseFromAddedClassesTable = (courseObj, courseType) => {
    console.log("removeCourseFromAddedClassesTable");

    var newAddedCourses = this.state.addedCourses;
    var newTakenCourses = this.state.takenCourses;
    var coursesArray;

    if (courseType == "departmentCourse") {
      coursesArray = newAddedCourses.addedDepartmentCourses;
      coursesArray = coursesArray.filter(
        course => course.courseCode !== courseObj.courseCode
      );
      newAddedCourses.addedDepartmentCourses = coursesArray;

      var newDepartmentCourses = this.enableCoursesButtons(
        courseObj,
        this.state.departmentCourses,
        courseType,
        courseObj.departmentCourseID
      );

      var newTakenDepartmentCourses = this.enableCoursesButtons(
        courseObj,
        this.state.takenCourses.takenDepartmentCourses,
        courseType,
        courseObj.departmentCourseID
      );

      newTakenCourses.takenDepartmentCourses = newTakenDepartmentCourses;

      this.setState({
        departmentCourses: newDepartmentCourses
      });
    } else if (courseType == "otherElectiveCourse") {
      coursesArray = newAddedCourses.addedOtherElectiveCourses;
      coursesArray = coursesArray.filter(
        course => course.courseCode !== courseObj.courseCode
      );
      newAddedCourses.addedOtherElectiveCourses = coursesArray;

      var newElectiveCourses = this.state.electiveCourses;

      var newOtherElectiveCourses = this.enableCoursesButtons(
        courseObj,
        newElectiveCourses.otherElectiveCourses,
        courseType,
        courseObj.otherElectiveCourseID
      );

      newElectiveCourses.otherElectiveCourses = newOtherElectiveCourses;

      var newTakenOtherElectiveCourses = this.enableCoursesButtons(
        courseObj,
        newTakenCourses.takenOtherElectiveCourses,
        courseType,
        courseObj.otherElectiveCourseID
      );

      newTakenCourses.takenOtherElectiveCourses = newTakenOtherElectiveCourses;

      this.setState({
        electiveCourses: newElectiveCourses
      });
    } else if (courseType == "commonElectiveCourse") {
      coursesArray = newAddedCourses.addedCommonElectiveCourses;
      coursesArray = coursesArray.filter(
        course => course.courseCode !== courseObj.courseCode
      );
      newAddedCourses.addedCommonElectiveCourses = coursesArray;

      var newElectiveCourses = this.state.electiveCourses;

      var newCommonElectiveCourses = this.enableCoursesButtons(
        courseObj,
        newElectiveCourses.commonElectiveCourses,
        courseType,
        courseObj.courseID
      );

      newElectiveCourses.commonElectiveCourses = newCommonElectiveCourses;

      var newTakenCommonElectiveCourses = this.enableCoursesButtons(
        courseObj,
        newTakenCourses.takenCommonElectiveCourses,
        courseType,
        courseObj.courseID
      );

      newTakenCourses.takenCommonElectiveCourses = newTakenCommonElectiveCourses;

      this.setState({
        electiveCourses: newElectiveCourses
      });
    }

    this.setState({
      addedCourses: newAddedCourses,
      takenCourses: newTakenCourses
    });
  };

  render() {
    console.log("this.state", this.state);
    if (this.state.isCourseRegistrationEnabled) {
      return (
        <div
          style={{
            backgroundColor: "white",
            height: "100vh"
          }}
        >
          <NavBar />
          <SideBar userInfo={this.props.location.userInfo} />
          <div className="remaining-akts">
            <div style={{ color: "rgb(46, 95, 173)" }}>
              AKTS: {this.state.remainingAkts}
            </div>
          </div>
          <FailedClassesAccordion
            updateRemainingAkts={this.updateRemainingAkts}
            updateTakenClasses={this.updateTakenClasses}
            takenClasses={this.state.takenClasses} // ?
            takenFailedDepartmentCourses={this.state.takenCourses.takenDepartmentCourses.filter(
              obj => !obj.isPassed
            )}
          />
          <DepartmentClassesAccordion
            updateRemainingAkts={this.updateRemainingAkts}
            departmentCourses={this.state.departmentCourses}
            userInfo={this.props.location.userInfo}
          />
          <ElectiveClassesAccordion
            updateRemainingAkts={this.updateRemainingAkts}
            takenElectiveCourses={this.state.takenCourses.takenOtherElectiveCourses.concat(
              this.state.takenCourses.takenCommonElectiveCourses
            )}
            commonElectiveClasses={
              this.state.electiveCourses.commonElectiveCourses
            }
            otherElectiveClasses={
              this.state.electiveCourses.otherElectiveCourses
            }
            userInfo={this.props.location.userInfo}
          />
          <TakenClassesAccordion // AddedClassesAccordion
            updateRemainingAkts={this.updateRemainingAkts}
            takenCourses={this.state.addedCourses.addedDepartmentCourses
              .concat(this.state.addedCourses.addedOtherElectiveCourses)
              .concat(this.state.addedCourses.addedCommonElectiveCourses)}
            removeCourseFromAddedClassesTable={
              this.removeCourseFromAddedClassesTable
            }
          />
          <Card className="confirm-course-card">
            <Card.Body>
              <Button
                className="confirm-course-registration"
                onClick={() => this.finalizeCourseRegistration()}
                block
              >
                Kaydımı Kesinleştir
              </Button>
            </Card.Body>
          </Card>
        </div>
      );
    } else {
      return (
        <div
          style={{
            backgroundColor: "white",
            height: "100vh"
          }}
        >
          <NavBar />
          <SideBar userInfo={this.props.location.userInfo} />{" "}
          <Container>
            <Row>
              <Col>
                <Card className="course-registration-disabled-card">
                  <Card.Header className="course-registration-disabled-title-text">
                    Uyarı
                  </Card.Header>
                  <Card.Body className="course-registration-disabled-text">
                    - Kaydınızı kesinleştirdiğiniz için ders kayıt işlemine
                    devam edemezsiniz.
                    <br></br>- Ders kayıt işlemine devam etmek için lütfen
                    danışmanınızla iletişime geçiniz.
                    <br></br>- Danışmanınızla iletişime geçmek için menüdeki
                    <mark class="red">Mesajlar</mark>bölümünü kullanabilirsiniz.
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}
