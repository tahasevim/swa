import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Table
} from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../courseRegistration.css";

const columns = [
  {
    dataField: "rank",
    text: "#",
    sort: true
  },
  {
    dataField: "removeProcesses",
    text: "İşlemler",
    sort: true
  },
  {
    dataField: "courseCode",
    text: "Ders Kodu",
    sort: true
  },
  {
    dataField: "opticCode",
    text: "Optik Kodu",
    sort: true
  },
  {
    dataField: "courseName",
    text: "Ders Adı",
    sort: true
  },
  {
    dataField: "section",
    text: "Şube",
    sort: true
  },
  {
    dataField: "mustOrElective",
    text: "Z/S",
    sort: true
  },
  {
    dataField: "theoryHour",
    text: "T",
    sort: true
  },
  {
    dataField: "praticsHour",
    text: "P",
    sort: true
  },
  {
    dataField: "credit",
    text: "K",
    sort: true
  },
  {
    dataField: "akts",
    text: "AKTS",
    sort: true
  }
  // {
  //   dataField: "status",
  //   text: "Durum",
  //   sort: true
  // }
];

// const rows = [
//   {
//     rank: 1,

//     courseCategory: "2",
//     termNum: "1.YY",
//     courseCode: "ECO135",
//     opticCode: 374135,
//     courseName: "İKTİSADA GİRİŞ I",
//     section: 2,
//     mustOrElective: "ZORUNLU",
//     theoryHour: 3,
//     praticsHour: 0,
//     credit: 3,
//     aktsNum: 5,
//     status: "YÜKSELTME"
//   }
// ];

export default class TakenClassesTable extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log("TakenClassesTable");
  //   console.log(nextProps.takenCourses);
  //   console.log(this.state.takenCourses);
  //   const rows = nextProps.takenCourses;
  //   if (rows != null && rows.length != 0) {
  //     var lastCourse = rows.slice(-1)[0];
  //     lastCourse.removeProcesses = (
  //       <div>
  //         <RemoveButton
  //           updateRemainingAkts={this.props.updateRemainingAkts}
  //           courseToBeRemoved={lastCourse}
  //           removeCourseFromTakenClassesTable={
  //             this.props.removeCourseFromTakenClassesTable
  //           }
  //         />
  //       </div>
  //     );
  //     this.setState({ takenCourses: rows });
  //   }
  // }

  finalTableRow = takenClasses => {
    var rank = 1;
    for (var course of takenClasses) {
      course.rank = rank;
      rank += 1;
    }
    return takenClasses;
  };

  render() {
    if (this.props.takenCourses.length == 0) {
      return (
        <Card>
          <Card.Body style={{ margin: "0 auto" }}>
            Sepetinizde hiç ders yok.
          </Card.Body>
        </Card>
      );
    } else {
      return (
        <div>
          <BootstrapTable
            bootstrap4
            keyField="id"
            data={this.finalTableRow(this.props.takenCourses)}
            columns={columns}
          />
        </div>
      );
    }
  }
}
