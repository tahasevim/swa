import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Tab,
  Tabs
} from "react-bootstrap";

import { Button } from "react-bootstrap";
import TakenClassesTable from "./TakenClassesTable";

import "../courseRegistration.css";

export default class TakenClassesAccordion extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log("TakenClassesAccordion", this.props.takenClasses);
    return (
      <div>
        <Accordion defaultActiveKey="0" className="must-course-accordion">
          <Card>
            <Card.Header>
              <Accordion.Toggle
                as={Button}
                className="must-course-accordion-item-header"
                variant="link"
                eventKey="1"
              >
                Alınan Dersler Sepetiniz
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
              <TakenClassesTable
                updateRemainingAkts={this.props.updateRemainingAkts}
                takenCourses={this.props.takenCourses}
                removeCourseFromTakenClassesTable={
                  this.props.removeCourseFromTakenClassesTable
                }
              />
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
    );
  }
}
