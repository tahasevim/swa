import React, { Component } from "react";
import { FormGroup, FormControl, FormLabel, FormText } from "react-bootstrap";
import "../assets/Sign/Sign.css";
//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
//import NavigationBar from "../NavBar/NavigationBar";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentNumber: "21527079",
      password: "12345678",
      email: "",
      sessionId: "",
      userId: "",
      isLoginFailError: "none"
    };
    this.errors = {
      loginFailError: "Kullanıcı adı veya şifre hatalı!"
    };
  }

  validateForm() {
    return (
      this.state.studentNumber.length > 0 && this.state.password.length > 0
    );
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  async checkAuthorization() {
    const { studentNumber, password } = this.state;
    console.log("checkAuth");
    console.log(studentNumber);
    console.log(password);
    return fetch("/api/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        studentNumber: studentNumber,
        password: password
      })
    }).then(response => {
      if (!response.ok) {
        console.log("auth fail");
        return false;
      } else {
        console.log("auth success");
        this.state.sessionId = response.headers.get("session-id");
        return response.json();
      }
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    var responseJSON = await this.checkAuthorization();

    if (responseJSON) {
      console.log("userInfos", responseJSON);
      this.state.userId = responseJSON.id;
      this.state.email = responseJSON.email;
      this.state.studentNumber = responseJSON.studentNumber;
      this.state.name = responseJSON.name;
      this.state.surname = responseJSON.surname;
      const { history } = this.props;
      history.push({
        pathname: "/home",
        userInfo: responseJSON
      });
    } else {
      console.log("wrong studentNumber or password");
      this.setState({ isLoginFailError: true });
    }

    // const productResponse = await fetch("/api/products");
    // the informations about announcements from the university
    // can be retrieved from MongoDB as another collection.
    // const products = await productResponse.json();
  };

  render() {
    // Handle visibility of NavigationBar components
    return (
      <div>
        <div className="Hacettepe-text">
          HACETTEPE ÜNİVERSİTESİ <br /> DERS KAYIT PROGRAMI{" "}
        </div>
        <div className="Login">
          <form onSubmit={this.handleSubmit} className="Login-view">
            <FormGroup controlId="studentNumber" bsSize="large">
              <FormLabel className="Label">
                <b>Öğrenci Numarası</b>
              </FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="studentNumber"
                value={this.state.studentNumber}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="password" bsSize="large">
              <FormLabel className="Label">
                {" "}
                <b>Şifre</b>{" "}
              </FormLabel>
              <FormControl
                className="Input"
                value={this.state.password}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <Button
              className="Login-button"
              variant="danger"
              type="submit"
              disabled={!this.validateForm()}
              block
            >
              GİRİŞ
            </Button>
            <div className="register-query">
              <div
                style={{
                  display: this.state.isLoginFailError,
                  color: "red",
                  marginTop: "3px"
                }}
              >
                {this.errors.loginFailError}
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
