import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Tabs,
  Tab
} from "react-bootstrap";

import NavBar from "../Bars/NavBar";
import SideBar from "../Bars/SideBar";
import TermCourses from "./TermCourses";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "courseCode",
    text: "Ders Kodu",
    sort: true
  },
  {
    dataField: "courseName",
    text: "Ders Adı",
    sort: true
  },
  {
    dataField: "section",
    text: "Şube",
    sort: true
  },
  {
    dataField: "credit",
    text: "Kredi",
    sort: true
  },
  {
    dataField: "akts",
    text: "AKTS",
    sort: true
  }
];

export default class TakenCoursesPage extends Component {
  constructor(props) {
    super(props);
    this.state = { addedCourses: [] };
  }

  async componentDidMount() {
    var response = await fetch("/api/user/added-courses", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        studentID: this.props.location.userInfo.studentID
      }
    });

    console.log(this.props.location.userInfo.studentID);

    const resp = await response.json();
    console.log(resp);

    var addedCourses = resp.shownDepartmentCourses
      .concat(resp.shownOtherElectiveCourses)
      .concat(resp.shownCommonElectiveCourses);

    this.setState({ addedCourses: addedCourses });
  }

  render() {
    console.log(this.state);
    console.log(this.props.location.academicYearInfo);
    return (
      <div
        className="TakenCoursesPage"
        style={{
          backgroundColor: "white",
          height: "100vh"
        }}
      >
        <NavBar />
        <SideBar userInfo={this.props.location.userInfo} />
        <div className="added-courses-table-container-parent">
          <div className="added-courses-table-container">
            <div className="added-courses-text">Eklenen Dersler</div>
            <BootstrapTable
              bootstrap4
              keyField="id"
              data={this.state.addedCourses}
              columns={columns}
            />
          </div>
        </div>
      </div>
    );
  }
}
