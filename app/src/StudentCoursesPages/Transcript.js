import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Tabs,
  Tab
} from "react-bootstrap";

import NavBar from "../Bars/NavBar";
import SideBar from "../Bars/SideBar";
import TermCourses from "./TermCourses";

export default class Transcript extends Component {
  constructor(props) {
    super(props);
    this.state = {
      yy1: [],
      yy2: [],
      yy3: [],
      yy4: [],
      yy5: [],
      yy6: [],
      yy7: [],
      yy8: []
    };
  }

  async componentDidMount() {
    var response = await fetch("/api/user/taken-courses", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        studentID: this.props.location.userInfo.studentID
      }
    });

    const resp = await response.json();
    console.log(resp);

    var yy1 = [],
      yy2 = [],
      yy3 = [],
      yy4 = [],
      yy5 = [],
      yy6 = [],
      yy7 = [],
      yy8 = [];

    this.parseCoursesToTerms(
      resp.shownTakenDepartmentCourses,
      yy1,
      yy2,
      yy3,
      yy4,
      yy5,
      yy6,
      yy7,
      yy8
    );

    this.parseCoursesToTerms(
      resp.shownTakenOtherElectiveCourses,
      yy1,
      yy2,
      yy3,
      yy4,
      yy5,
      yy6,
      yy7,
      yy8
    );

    this.parseCoursesToTerms(
      resp.shownTakenCommonElectiveCourses,
      yy1,
      yy2,
      yy3,
      yy4,
      yy5,
      yy6,
      yy7,
      yy8
    );

    this.setState({
      yy1: yy1,
      yy2: yy2,
      yy3: yy3,
      yy4: yy4,
      yy5: yy5,
      yy6: yy6,
      yy7: yy7,
      yy8: yy8
    });
  }

  parseCoursesToTerms(arr, yy1, yy2, yy3, yy4, yy5, yy6, yy7, yy8) {
    for (var course of arr) {
      if (course.term == "1.YY") {
        yy1.push(course);
      } else if (course.term == "2.YY") {
        yy2.push(course);
      } else if (course.term == "3.YY") {
        yy3.push(course);
      } else if (course.term == "4.YY") {
        yy4.push(course);
      } else if (course.term == "5.YY") {
        yy5.push(course);
      } else if (course.term == "6.YY") {
        yy6.push(course);
      } else if (course.term == "7.YY") {
        yy7.push(course);
      } else if (course.term == "8.YY") {
        yy8.push(course);
      }
    }
  }

  render() {
    console.log(this.state);
    return (
      <div
        className="Transcript"
        style={{
          backgroundColor: "white",
          height: "100vh"
        }}
      >
        <NavBar />
        <SideBar userInfo={this.props.location.userInfo} />
        <div className="term-courses-container">
          <div className="transcript-text">Transkript</div>

          <div className="term-courses">
            <TermCourses termName="1.YY" termCourses={this.state.yy1} />
          </div>
          <div className="term-courses">
            <TermCourses termName="2.YY" termCourses={this.state.yy2} />
          </div>
          <div className="term-courses">
            <TermCourses termName="3.YY" termCourses={this.state.yy3} />
          </div>
          <div className="term-courses">
            <TermCourses termName="4.YY" termCourses={this.state.yy4} />
          </div>
          <div className="term-courses">
            <TermCourses termName="5.YY" termCourses={this.state.yy5} />
          </div>
          <div className="term-courses">
            <TermCourses termName="6.YY" termCourses={this.state.yy6} />
          </div>
          <div className="term-courses">
            <TermCourses termName="7.YY" termCourses={this.state.yy7} />
          </div>
          <div className="term-courses">
            <TermCourses termName="8.YY" termCourses={this.state.yy8} />
          </div>
        </div>
      </div>
    );
  }
}
