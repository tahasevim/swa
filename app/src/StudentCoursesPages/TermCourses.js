import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col,
  Accordion,
  Table
} from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";

import { Button } from "react-bootstrap";
import "../CourseRegistration/courseRegistration.css";

const columns = [
  {
    dataField: "courseCode",
    text: "Ders Kodu",
    sort: true
  },
  {
    dataField: "courseName",
    text: "Ders Adı",
    sort: true
  },
  {
    dataField: "theoryHour",
    text: "T",
    sort: true
  },
  {
    dataField: "praticsHour",
    text: "P",
    sort: true
  },
  {
    dataField: "credit",
    text: "K",
    sort: true
  },
  {
    dataField: "akts",
    text: "AKTS",
    sort: true
  },
  {
    dataField: "finalGrade",
    text: "G.N",
    sort: true
  },
  {
    dataField: "makeUpGrade",
    text: "B.N",
    sort: true
  }
];

export default class TermCourses extends Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, rows: [] };
  }

  render() {
    if (this.props.termCourses.length > 0) {
      return (
        <div>
          <div className="term-name">{this.props.termName}</div>
          <BootstrapTable
            bootstrap4
            keyField="id"
            data={this.props.termCourses}
            columns={columns}
          />
          <hr class="new1" />
        </div>
      );
    } else {
      return <div></div>;
    }
  }
}
