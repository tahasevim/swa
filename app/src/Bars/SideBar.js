import React, { Component } from "react";
import "../assets/HomePage/sidebar.css";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";

export default class SideBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="sidebar">
        <Menu isOpen={true} noOverlay disableAutoFocus noTransition>
          <Link
            id="home"
            className="menu-item"
            to={{ pathname: "/home", userInfo: this.props.userInfo }}
          >
            Anasayfa
          </Link>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/student-informations",
              userInfo: this.props.userInfo
            }}
          >
            Kullanıcı İşlemleri
          </Link>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/transcript",
              userInfo: this.props.userInfo
            }}
          >
            Transkript
          </Link>

          <Link
            id="home"
            className="menu-item"
            to={{ pathname: "/message", userInfo: this.props.userInfo }}
          >
            Mesajlar
          </Link>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/course-registration",
              userInfo: this.props.userInfo
            }}
          >
            Ders Kayıt
          </Link>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/taken-courses",
              userInfo: this.props.userInfo
            }}
          >
            Eklenen Dersler
          </Link>
        </Menu>
      </div>
    );
  }
}
