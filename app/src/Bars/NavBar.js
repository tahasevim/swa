import React, { Component } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import "../assets/Bars/navbar.css";
import userIcon from "../assets/images/logout.png";
import hacettepeIcon from "../assets/images/hacettepe.png";
import { NavLink, Link } from "react-router-dom";

export default class NavigationBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  handleOpen = () => {
    this.setState({ isOpen: true });
  };

  logOut = async () => {
    console.log("logOut");
    var userInfo = this.props.userInfo;
    fetch("/api/logout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
        //"session-id": userInfo.sessionId
      },
      body: JSON.stringify({
        //id: userInfo.userId,
        type: "customer"
      })
    });
  };

  //   editAccount = () => {
  //     this.props.history.push({
  //       pathname: "/account",
  //       userInfo: this.props.userInfo,
  //       mode: this.props.mode,
  //       auth: true,
  //       products: this.props.products
  //     });
  //   };

  render() {
    return (
      <div className="navbar-main">
        <Navbar collapseOnSelect expand="lg" className="navbar-color">
          <Link
            to={{
              pathname: "/home",
              userInfo: this.props.userInfo
            }}
          >
            <Navbar.Brand>
              <div className="homepage-brand">
                <img src={hacettepeIcon} className="deer-img" />
                <strong> Hacettepe Üniversitesi</strong>
              </div>
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto" />
            <Nav className="right-container">
              <Link to={{ pathname: "/" }}>
                <img src={userIcon} className="cart" onClick={this.logOut} />
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
