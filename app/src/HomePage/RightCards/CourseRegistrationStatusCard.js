import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import "../../assets/HomePage/sidebar.css";
import "../../assets/HomePage/homepage.css";

export default class CourseRegistrationStatusCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    // var response = await fetch("/api/products");
    // const productBody = await response.json();
    // this.setState({ products: productBody });
  }

  render() {
    return (
      <div>
        <Card className="course-registration-main-informations-card">
          <Card.Title className="course-registration-main-informations-title">
            Uyarılar
          </Card.Title>
          <Card.Body className="course-registration-main-informations">
            <Card className="course-registration-status-card">
              <Card.Title className="course-registration-status-title">
                Anadal Ders Kayıt Durumu
              </Card.Title>
              <Card.Body className="course-registration-status">
                {this.props.userInfo.courseRegistrationConfirmed
                  ? "Ders Kaydı Onaylandı."
                  : "Ders Kaydı Onaylanmadı."}
              </Card.Body>
            </Card>

            <Card className="course-registration-date-card">
              <Card.Title className="course-registration-date-title">
                Ders Kayıt Tarihi
              </Card.Title>
              <Card.Body className="course-registration-date">
                {this.props.academicYearInfo.courseRegistrationDate}
              </Card.Body>
              {/* In another card : {this.props.academicYearInfo.addDropDate} */}
            </Card>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
