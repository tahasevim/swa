import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import AnnouncementList from "./AnnouncementList";
import { Scrollbars } from "react-custom-scrollbars";

import "../../../assets/HomePage/sidebar.css";
import "../../../assets/HomePage/homepage.css";

//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
//import NavigationBar from "../NavBar/NavigationBar";
import SideBar from "../../../Bars/SideBar";

export default class AnnouncementContainerCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      announcements: []
    };
  }

  async componentWillMount() {
    var response = await fetch("/api/university/annons");
    const announcements = await response.json();
    this.setState({ announcements: announcements });
  }

  render() {
    console.log(this.state.announcements);
    return (
      <div>
        <Card className="student-announcement-card">
          <Card.Title className="student-announcement-title">
            Duyurular
          </Card.Title>
          <Card.Body className="student-announcement">
            <Scrollbars className="scroll-bar">
              <Card className="scroll-bar-card">
                <AnnouncementList announcements={this.state.announcements} />
              </Card>
            </Scrollbars>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
