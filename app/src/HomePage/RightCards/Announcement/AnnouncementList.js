import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import NavBar from "../../../Bars/NavBar";
import AnnouncementElement from "./AnnouncementElement";

import "../../../assets/HomePage/sidebar.css";
import "../../../assets/HomePage/homepage.css";

//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
//import NavigationBar from "../NavBar/NavigationBar";
import SideBar from "../../../Bars/SideBar";

export default class AnnouncementList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.announcements.map(a => (
          <AnnouncementElement date={a.date} announcement={a.announcement} />
        ))}
      </div>
    );
  }
}
