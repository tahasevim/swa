import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import NavBar from "../../../Bars/NavBar";
import "../../../assets/HomePage/sidebar.css";
import "../../../assets/HomePage/homepage.css";

//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
//import NavigationBar from "../NavBar/NavigationBar";
import SideBar from "../../../Bars/SideBar";

export default class AnnouncementElement extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Card className="student-announcement-element-card">
          <Card.Body className="student-announcement-element">
            <div className="student-announcement-element-date">
              {this.props.date}
            </div>{" "}
            {this.props.announcement}
          </Card.Body>
        </Card>
      </div>
    );
  }
}
