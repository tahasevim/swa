import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import NavBar from "../../Bars/NavBar";
import "../../assets/HomePage/sidebar.css";
import "../../assets/HomePage/homepage.css";

//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
//import NavigationBar from "../NavBar/NavigationBar";
import SideBar from "../../Bars/SideBar";

export default class StudentInformationCard extends Component {
  constructor(props) {
    super(props);
    this.state = { departmentInfo: {}, advisorInfo: {} };
    this.errors = {};
  }

  async componentDidMount() {
    var response = await fetch("/api/department", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        departmentID: this.props.userInfo.departmentID,
        advisorID: this.props.userInfo.advisorID
      }
    });

    const resp = await response.json();
    console.log(resp);

    this.setState({
      departmentInfo: resp.department,
      advisorInfo: resp.advisor
    });
  }

  render() {
    return (
      <div>
        <Card className="student-info-card">
          <Card.Title className="student-info-title">Bilgilerim</Card.Title>
          <Card.Body className="student-info">
            <Card className="student-term-card">
              <Card.Title className="student-term-title">Dönem</Card.Title>
              <Card.Body className="student-term">
                {this.props.academicYearInfo.academicYear}
              </Card.Body>
            </Card>

            <Card className="student-creditType-card">
              <Card.Title className="student-creditType-title">
                Kredi Türü
              </Card.Title>
              <Card.Body className="student-creditType">
                {this.props.userInfo.subjectedCreditType}
              </Card.Body>
            </Card>

            <Card className="student-major-card">
              <Card.Title className="student-major-title">Anadal</Card.Title>
              <Card.Body className="student-major">
                <Card className="student-faculty-card">
                  <Card.Title className="student-faculty-title">
                    {this.props.userInfo.subjectedCreditType}
                  </Card.Title>
                  <Card.Body className="student-faculty">
                    {this.state.departmentInfo.facultyName}
                  </Card.Body>
                </Card>

                <Card className="student-program-card">
                  <Card.Title className="student-program-title">
                    Program
                  </Card.Title>
                  <Card.Body className="student-program">
                    {this.state.departmentInfo.departmentName}
                  </Card.Body>
                </Card>

                <Card className="student-grade-card">
                  <Card.Title className="student-grade-title">Sınıf</Card.Title>
                  <Card.Body className="student-grade">4</Card.Body>
                </Card>

                <Card className="student-advisor-card">
                  <Card.Title className="student-advisor-title">
                    Danışman
                  </Card.Title>
                  <Card.Body className="student-advisor">
                    {this.state.advisorInfo.academicRank}{" "}
                    {this.state.advisorInfo.name}{" "}
                    {this.state.advisorInfo.surname}
                  </Card.Body>
                </Card>
              </Card.Body>
            </Card>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
