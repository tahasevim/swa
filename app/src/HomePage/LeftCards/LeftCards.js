import React, { Component } from "react";
import { Col } from "react-bootstrap";
import StudentInformationCard from "./StudentInformationCard";
import "../../assets/HomePage/sidebar.css";
import "../../assets/HomePage/homepage.css";

export default class LeftCards extends Component {
  constructor(props) {
    super(props);
    this.errors = {};
  }

  render() {
    return (
      <div>
        <Col>
          <StudentInformationCard
            userInfo={this.props.userInfo}
            academicYearInfo={this.props.academicYearInfo}
          />
        </Col>
      </div>
    );
  }
}
