import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import StudentInformationCard from "./LeftCards/StudentInformationCard";
import NavBar from "../Bars/NavBar";
import "../assets/HomePage/sidebar.css";
import "../assets/HomePage/homepage.css";

//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
//import NavigationBar from "../NavBar/NavigationBar";
import SideBar from "../Bars/SideBar";

export default class WelcomeCard extends Component {
  constructor(props) {
    super(props);
    this.userInfo = this.props.userInfo;
    this.errors = {};
  }

  render() {
    console.log("WelcomeCard", this.userInfo);
    return (
      <div>
        <Col>
          <Card className="welcome-text-card">
            <Card.Body className="welcome-text">
              {" "}
              {this.userInfo.studentNumber} - {this.userInfo.name}{" "}
              {this.userInfo.surname}
            </Card.Body>
          </Card>
        </Col>
      </div>
    );
  }
}
