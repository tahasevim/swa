import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Card,
  Container,
  Row,
  Col
} from "react-bootstrap";

import WelcomeCard from "./WelcomeCard";
import LeftCards from "./LeftCards/LeftCards";
import RightCards from "./RightCards/RightCards";

import NavBar from "../Bars/NavBar";
import "../assets/HomePage/sidebar.css";
import "../assets/HomePage/homepage.css";

//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
//import NavigationBar from "../NavBar/NavigationBar";
import SideBar from "../Bars/SideBar";

export default class HomePage extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.userInfo = props.location.userInfo;
    this.state = { academicYearInfo: {} };
  }

  async componentDidMount() {
    var response = await fetch("/api/university/academic-year");
    const academicYearInfo = await response.json();

    this.setState({
      academicYearInfo: academicYearInfo
    });
  }

  render() {
    console.log("AcademicInfo", this.state.academicYearInfo);
    console.log("Department", this.state.departmentInfo);
    console.log("Advisor", this.state.advisorInfo);

    // Handle visibility of NavigationBar components
    return (
      <div
        style={{
          backgroundColor: "white",
          height: "100vh"
        }}
      >
        <NavBar />
        <SideBar userInfo={this.userInfo} />
        <Container>
          <Row>
            <WelcomeCard userInfo={this.userInfo} />
          </Row>
          <Row>
            <LeftCards
              userInfo={this.userInfo}
              academicYearInfo={this.state.academicYearInfo}
            />
            <RightCards
              userInfo={this.userInfo}
              academicYearInfo={this.state.academicYearInfo}
            />
          </Row>
        </Container>
      </div>
    );
  }
}
