import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  Button,
  Container,
  Row,
  Col,
  Card,
  Accordion
} from "react-bootstrap";
import NavBar from "../Bars/NavBar";
import SideBar from "../Bars/SideBar";
import "./account.css";

const emailValidationRegExp =
  "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentInfo: {},
      advisorInfo: {},
      advisorRankNameSurname: "",
      email: "",
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
      isPasswordWrongError: "none",
      isPasswordMatchError: "none",
      isPasswordLengthError: "none",
      isEmailValidityError: "none",
      isPasswordUpdateSuccess: "none",
      isEmailUpdateSuccess: "none"
    };

    this.errors = {
      passwordWrongError: "Yanlış parola!",
      passwordMatchError: "Parolalar eşleşmemektedir!",
      passwordLengthError: "Parola en az 8 karakter olmalıdır!",
      emailValidityError: "Geçerli bir e-posta adresi giriniz!"
    };

    this.successes = {
      updateSuccess: "Güncelleme başarılı!"
    };
  }

  async componentWillMount() {
    var response = await fetch("/api/department", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        departmentID: this.props.location.userInfo.departmentID,
        advisorID: this.props.location.userInfo.advisorID
      }
    });

    const resp = await response.json();
    console.log(resp);

    this.setState({
      departmentInfo: resp.department,
      advisorInfo: resp.advisor,
      advisorRankNameSurname:
        resp.advisor.academicRank +
        " " +
        resp.advisor.name +
        " " +
        resp.advisor.surname
    });
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleEmailChange = event => {
    var isValidEmail = this.emailValidityCheck();
    console.log(isValidEmail);
    this.setState({
      [event.target.id]: event.target.value,
      isEmailValidityError: isValidEmail == false ? true : "none"
    });
  };

  emailValidityCheck() {
    var regex = RegExp(emailValidationRegExp);
    var isValidEmail = regex.test(this.state.email);

    return isValidEmail;
  }

  validatePasswordForm() {
    var isValid =
      this.state.currentPassword.length > 0 &&
      this.state.newPassword.length > 0 &&
      this.state.confirmPassword.length > 0;

    return isValid;
  }

  validateEmailForm() {
    var isValid = this.state.isEmailValidityError == "none" ? true : false;
    isValid &= this.state.email.length > 0;
    return isValid;
  }

  async updatePasswordInfo() {
    const { currentPassword, newPassword } = this.state;
    return fetch("/api/user/account/password", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
        //"session-id": userInfo.sessionId,
        //aid: "emp"
      },
      body: JSON.stringify({
        oldPassword: currentPassword,
        newPassword: newPassword,
        id: this.props.location.userInfo.studentID
      })
    });
  }

  handlePasswordSubmit = async event => {
    event.preventDefault();
    const { newPassword, confirmPassword } = this.state;
    if (newPassword.length < 8) {
      console.log("The password length is less than 8.");
      this.setState({
        isPasswordWrongError: "none",
        isPasswordMatchError: "none",
        isPasswordLengthError: true,
        isPasswordUpdateSuccess: "none"
      });
      return false;
    } else {
      if (newPassword !== confirmPassword) {
        this.setState({
          isPasswordWrongError: "none",
          isPasswordMatchError: true,
          isPasswordLengthError: "none",
          isPasswordUpdateSuccess: "none"
        });
        return false;
      } else {
        var responseJSON = await this.updatePasswordInfo();
        console.log(responseJSON);
        if (responseJSON.status == 401) {
          console.log(
            "The password is not matched with the one in the database"
          );
          this.setState({
            isPasswordWrongError: true,
            isPasswordMatchError: "none",
            isPasswordLengthError: "none",
            isPasswordUpdateSuccess: "none"
          });
          return false;
        } else if (responseJSON.status == 201) {
          this.setState({
            isPasswordWrongError: "none",
            isPasswordMatchError: "none",
            isPasswordLengthError: "none",
            isPasswordUpdateSuccess: true
          });
          console.log("Update is successful.");
          return true;
        } else {
          alert("update is failed for some reasons..");
          console.log("Update is failed.");
        }
      }
    }
  };

  async updateEmailInfo() {
    return fetch("/api/user/account/email", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
        //"session-id": userInfo.sessionId,
        //aid: "emp"
      },
      body: JSON.stringify({
        id: this.props.location.userInfo.studentID,
        email: this.state.email
      })
    });
  }

  handleEmailSubmit = async event => {
    event.preventDefault();
    var responseJSON = await this.updateEmailInfo();
    console.log(responseJSON);
    if (responseJSON.status == 201) {
      this.setState({
        isEmailUpdateSuccess: true
      });
      console.log("Email Update is successful.");
      this.props.location.userInfo.email = this.state.email;
      return true;
    } else {
      alert("update is failed for some reasons..");
      console.log("Update is failed.");
    }
  };

  render() {
    console.log(this.state.advisorInfo);

    return (
      <div
        className="Account"
        style={{
          backgroundColor: "white",
          height: "100vh"
        }}
      >
        <NavBar />
        <SideBar userInfo={this.props.location.userInfo} />
        <Container>
          <Row>
            <Col>
              <form>
                <FormGroup controlId="name" bsSize="large">
                  <FormLabel className="Label"> Ad</FormLabel>
                  <FormControl
                    className="Input"
                    //value={this.state.currentPassword}
                    defaultValue={this.props.location.userInfo.name}
                    onChange={this.handleChange}
                    type="name"
                    disabled
                  />
                </FormGroup>
                <FormGroup controlId="surname" bsSize="large">
                  <FormLabel className="Label"> Soyad</FormLabel>
                  <FormControl
                    className="Input"
                    //value={this.state.currentPassword}
                    defaultValue={this.props.location.userInfo.surname}
                    onChange={this.handleChange}
                    type="surname"
                    disabled
                  />
                </FormGroup>
                <FormGroup controlId="studentNumber" bsSize="large">
                  <FormLabel className="Label"> Okul Numarası</FormLabel>
                  <FormControl
                    className="Input"
                    //value={this.state.currentPassword}
                    defaultValue={this.props.location.userInfo.studentNumber}
                    onChange={this.handleChange}
                    type="studentNumber"
                    disabled
                  />
                </FormGroup>
                <FormGroup controlId="studentFaculty" bsSize="large">
                  <FormLabel className="Label"> Fakülte</FormLabel>
                  <FormControl
                    className="Input"
                    defaultValue={this.state.departmentInfo.facultyName}
                    onChange={this.handleChange}
                    type="studentFaculty"
                    disabled
                  />
                </FormGroup>
                <FormGroup controlId="studentDepartment" bsSize="large">
                  <FormLabel className="Label"> Bölümü</FormLabel>
                  <FormControl
                    className="Input"
                    defaultValue={this.state.departmentInfo.departmentName}
                    onChange={this.handleChange}
                    type="studentDepartment"
                    disabled
                  />
                </FormGroup>
                <FormGroup controlId="studentGrade" bsSize="large">
                  <FormLabel className="Label"> Sınıf</FormLabel>
                  <FormControl
                    className="Input"
                    defaultValue={this.props.location.userInfo.year} // Change
                    onChange={this.handleChange}
                    type="studentGrade"
                    disabled
                  />
                </FormGroup>
                <FormGroup controlId="studentGrade" bsSize="large">
                  <FormLabel className="Label"> Dönem</FormLabel>
                  <FormControl
                    className="Input"
                    defaultValue={this.props.location.userInfo.term}
                    onChange={this.handleChange}
                    type="studentGrade"
                    disabled
                  />
                </FormGroup>
                <FormGroup
                  controlId="studentCourseRegistrationStatus"
                  bsSize="large"
                >
                  <FormLabel className="Label"> Ders Kaydı Durumu</FormLabel>
                  <FormControl
                    className={
                      this.props.location.userInfo.courseRegistrationConfirmed
                        ? "course-registration-status-positive"
                        : "course-registration-status-negative"
                    }
                    defaultValue={
                      this.props.location.userInfo.courseRegistrationConfirmed
                        ? "Onaylı"
                        : "Onaysız"
                    }
                    onChange={this.handleChange}
                    type="studentCourseRegistrationStatus"
                    disabled
                  ></FormControl>
                </FormGroup>

                <FormGroup controlId="studentAdvisor" bsSize="large">
                  <FormLabel className="Label"> Danışman</FormLabel>
                  <FormControl
                    className="Input"
                    defaultValue={this.state.advisorRankNameSurname}
                    // Change
                    onChange={this.handleChange}
                    type="studentAdvisor"
                    disabled
                  />
                </FormGroup>
              </form>
            </Col>
            <Col>
              <Accordion
                defaultActiveKey="0"
                className="change-email-accordion"
              >
                <Card>
                  <Card.Header>
                    <Accordion.Toggle
                      as={Button}
                      className="change-email-accordion-header"
                      variant="link"
                      eventKey="1"
                    >
                      E-posta güncelleme
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="1">
                    <div>
                      <FormGroup
                        controlId="email"
                        bsSize="large"
                        className="change-email-form"
                      >
                        <FormLabel className="Label">E-posta</FormLabel>
                        <FormControl
                          className="Input"
                          autoFocus
                          type="email"
                          defaultValue={this.props.location.userInfo.email}
                          onChange={this.handleEmailChange}
                        />
                      </FormGroup>
                      <div
                        className="email-notvalid-error-text"
                        style={{
                          display: this.state.isEmailValidityError
                        }}
                      >
                        {this.errors.emailValidityError}
                      </div>
                      <div className="email-update-button-container">
                        <Button
                          className="email-update-button"
                          onClick={this.handleEmailSubmit}
                          disabled={!this.validateEmailForm()}
                        >
                          Güncelle
                        </Button>
                      </div>
                      <div
                        className="email-update-success-text"
                        style={{
                          display: this.state.isEmailUpdateSuccess
                        }}
                      >
                        <b>{this.successes.updateSuccess}</b>
                      </div>
                    </div>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
              <Accordion
                defaultActiveKey="2"
                className="change-password-accordion"
              >
                <Card>
                  <Card.Header>
                    <Accordion.Toggle
                      as={Button}
                      className="change-password-accordion-header"
                      variant="link"
                      eventKey="1"
                    >
                      Parola güncelleme
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="1">
                    <div>
                      <FormGroup
                        controlId="currentPassword"
                        bsSize="large"
                        className="change-password-form"
                      >
                        <FormLabel className="Label"> Mevcut Parola</FormLabel>
                        <FormControl
                          className="Input"
                          value={this.state.currentPassword}
                          onChange={this.handleChange}
                          type="password"
                        />
                      </FormGroup>
                      <div
                        className="password-wrong-error-text"
                        style={{
                          display: this.state.isPasswordWrongError
                        }}
                      >
                        {this.errors.passwordWrongError}
                      </div>
                      <FormGroup
                        controlId="newPassword"
                        bsSize="large"
                        className="change-password-form"
                      >
                        <FormLabel className="Label"> Yeni Parola</FormLabel>
                        <FormControl
                          className="Input"
                          value={this.state.newPassword}
                          onChange={this.handleChange}
                          type="password"
                        />
                      </FormGroup>
                      <div
                        className="password-length-error-text"
                        style={{
                          display: this.state.isPasswordLengthError
                        }}
                      >
                        {this.errors.passwordLengthError}
                      </div>
                      <FormGroup
                        controlId="confirmPassword"
                        bsSize="large"
                        className="change-password-form"
                      >
                        <FormLabel className="Label">
                          {" "}
                          Yeni Parola Doğrulaması
                        </FormLabel>
                        <FormControl
                          className="Input"
                          value={this.state.confirmPassword}
                          onChange={this.handleChange}
                          type="password"
                        />
                      </FormGroup>
                      <div
                        className="password-match-error-text"
                        style={{
                          display: this.state.isPasswordMatchError
                        }}
                      >
                        {this.errors.passwordMatchError}
                      </div>
                      <div className="password-update-button-container">
                        <Button
                          className="password-update-button"
                          onClick={this.handlePasswordSubmit}
                          disabled={!this.validatePasswordForm()}
                        >
                          Güncelle
                        </Button>
                      </div>
                      <div
                        className="password-update-success-text"
                        style={{
                          display: this.state.isPasswordUpdateSuccess
                        }}
                      >
                        <b>{this.successes.updateSuccess}</b>
                      </div>
                    </div>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
