import React, { Component } from "react";
import { FormGroup, FormControl, FormLabel, FormText } from "react-bootstrap";
import "../assets/Sign/Sign.css";
//import { userService} from "./userService"; //WIP
import { Button } from "react-bootstrap";
//import NavigationBar from "../NavBar/NavigationBar";
import Login from "../Login/Login";
import HomePage from "../HomePage/HomePage";
import Main from "./Main";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { backgroundImg: require("../assets/images/background.svg") };
    this.errors = {};
  }

  render() {
    return (
      <div
        style={{
          backgroundImage: "url(" + this.state.backgroundImg + ")",
          backgroundSize: "50% 100%",
          backgroundRepeat: "no-repeat",
          height: "100vh"
        }}
      >
        <Main />
      </div>
    );
  }
}
