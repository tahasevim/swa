import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "../Login/Login";
import HomePage from "../HomePage/HomePage";
import Account from "../Account/Account";
import Message from "../Message/Message";
import CourseRegistration from "../CourseRegistration/CourseRegistration";
import React from "react";
import Transcript from "../StudentCoursesPages/Transcript";
import TakenCoursesPage from "../StudentCoursesPages/TakenCoursesPage";

const Main = () => (
  <main>
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/home" component={HomePage} />
        <Route path="/student-informations" component={Account} />
        <Route path="/message" component={Message} />
        <Route path="/course-registration" component={CourseRegistration} />
        <Route path="/transcript" component={Transcript} />
        <Route path="/taken-courses" component={TakenCoursesPage} />

        {/*
        <Route path="/products/:productId" component={ProductDetail} /> */}
      </Switch>
    </Router>
  </main>
);

export default Main;
