import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  FormText,
  Form,
  Card
} from "react-bootstrap";
import Navbar from "../Bars/NavBar";
import Login from "../Login/Login";
import SideBar from "../Bars/SideBar";
import { Scrollbars } from "react-custom-scrollbars";
import { Widget, addResponseMessage } from "react-chat-widget";
import "react-chat-widget/lib/styles.css";

import "./message.css";
import { Button, Container, Row, Col } from "react-bootstrap";

export default class Message extends Login {
  // Fix, just temp
  constructor(props) {
    super(props);
    this.state = {
      messageTopic: "",
      messageContent: "",
      from: this.props.location.userInfo.studentID,
      to:
        this.props.location.userInfo.studentID ===
        "dc12a6a2-a41b-4c14-b0b0-2ffd9d6ffa33"
          ? ""
          : "dc12a6a2-a41b-4c14-b0b0-2ffd9d6ffa33"
    };
    this.ws = new WebSocket(
      "ws://167.172.173.250:30081/chat?name=" + this.state.from
    );
  }

  async componentDidMount() {
    this.ws.onopen = () => {
      console.log("Connected to websocket server");
    };
    this.ws.onmessage = event => {
      const message = JSON.parse(event.data);
      if (this.state.to === "") this.setState({ to: message.from });
      addResponseMessage(message.content);
    };
    this.ws.onclose = () => {
      "Disconnected from websocket server";
    };
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleNewUserMessage = newMessage => {
    console.log(`New message incoming! ${newMessage}`);
    // Now send the message throught the backend API
    this.ws.send(
      JSON.stringify({
        from: this.state.from,
        to: this.state.to,
        content: newMessage
      })
    );
  };

  render() {
    return (
      <div
        style={{
          backgroundColor: "white",
          height: "100vh"
        }}
        className="Message"
      >
        <Navbar />
        <SideBar userInfo={this.props.location.userInfo} />
        <Container>
          <Row>
            <Card>
              <Col>
                <form>
                  <Form>
                    <Form.Group as={Row} controlId="formPlaintextEmail">
                      <Form.Label column sm="2">
                        Kime
                      </Form.Label>
                      <Col sm="10">
                        <Form.Control
                          plaintext
                          readOnly
                          defaultValue={this.state.advisorRankNameSurname}
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="messageTopic">
                      <Form.Label column sm="2">
                        Konu
                      </Form.Label>
                      <Col sm="10">
                        <Form.Control
                          className="topic-content-form"
                          type="topic"
                          value={this.state.messageTopic}
                          placeholder=""
                          onChange={this.handleChange}
                        />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="messageContent">
                      <Form.Label column sm="2">
                        Mesaj İçeriği
                      </Form.Label>
                      <Col sm="10">
                        <textarea
                          class="form-control"
                          rows="15"
                          id="messageContent"
                          value={this.state.messageContent}
                          onChange={this.handleChange}
                          name="text"
                        ></textarea>
                      </Col>
                    </Form.Group>
                  </Form>
                  <div className="button-container">
                    <Button
                      className="send-message-button"
                      variant="primary"
                      type="submit"
                    >
                      Mesaj Gönder
                    </Button>
                  </div>
                </form>
              </Col>
            </Card>
            <Col>
              <Widget
                autofocus={false}
                title="Danışmanınıza anlık mesaj gönderin."
                subtitle=""
                handleNewUserMessage={this.handleNewUserMessage}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
